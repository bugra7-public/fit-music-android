package com.dobra.fitmusic.service

import android.app.Notification
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.support.v4.media.MediaDescriptionCompat
import android.support.v4.media.session.MediaSessionCompat
import androidx.core.app.NotificationCompat.PRIORITY_LOW
import com.dobra.fitmusic.R
import com.dobra.fitmusic.adapter.PlayerNotificationAdapter
import com.dobra.fitmusic.manager.MusicPlayerManager
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector
import com.google.android.exoplayer2.ext.mediasession.TimelineQueueNavigator
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import com.google.android.exoplayer2.util.NotificationUtil.IMPORTANCE_NONE
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val MEDIA_SESSION_TAG = "FIT_MUSIC_MEDIA_SESSION_TAG"
private const val NOTIFICATION_ID = 1994

@AndroidEntryPoint
class MusicPlayerService : Service() {
    @Inject
    lateinit var musicPlayerManager: MusicPlayerManager

    private lateinit var playerNotificationManager: PlayerNotificationManager
    private lateinit var mediaSessionCompat: MediaSessionCompat
    private lateinit var mediaSessionConnector: MediaSessionConnector

    override fun onCreate() {
        super.onCreate()
        playerNotificationManager = PlayerNotificationManager
            .Builder(this, NOTIFICATION_ID, getString(R.string.NotificationChannelId))
            .setMediaDescriptionAdapter(PlayerNotificationAdapter(this))
            .setChannelImportance(IMPORTANCE_NONE)
            .setNotificationListener(object : PlayerNotificationManager.NotificationListener {
                override fun onNotificationPosted(
                    notificationId: Int,
                    notification: Notification,
                    ongoing: Boolean
                ) {
                    super.onNotificationPosted(notificationId, notification, ongoing)
                    if (ongoing) {
                        startForeground(notificationId, notification)
                    }
                }

                override fun onNotificationCancelled(
                    notificationId: Int,
                    dismissedByUser: Boolean
                ) {
                    super.onNotificationCancelled(notificationId, dismissedByUser)
                    stopSelf()
                }
            })
            .setChannelNameResourceId(R.string.NotificationChannelName)
            .setChannelDescriptionResourceId(R.string.NotificationChannelDescription)
            .setStopActionIconResourceId(R.drawable.ic_exit)
            .setSmallIconResourceId(R.drawable.ic_music_notification)
            .build()
        mediaSessionCompat = MediaSessionCompat(this, MEDIA_SESSION_TAG)
        mediaSessionCompat.isActive = true
        mediaSessionConnector = MediaSessionConnector(mediaSessionCompat)
        playerNotificationManager.setMediaSessionToken(mediaSessionCompat.sessionToken)
        playerNotificationManager.setPlayer(musicPlayerManager.exoPlayer)
        playerNotificationManager.setPriority(PRIORITY_LOW)
        playerNotificationManager.setUseFastForwardAction(false)
        playerNotificationManager.setUseRewindAction(false)
        playerNotificationManager.setUseChronometer(true)
        playerNotificationManager.setUseStopAction(true)

        /* makes use of the play next/previous buttons of bluetooth headsets, google assistant etc.
        read for more:
        https://medium.com/google-exoplayer/the-mediasession-extension-for-exoplayer-82b9619deb2d
        */
        val timelineQueueNavigator = object : TimelineQueueNavigator(mediaSessionCompat) {
            override fun getMediaDescription(
                player: Player,
                windowIndex: Int
            ): MediaDescriptionCompat {
                return MediaDescriptionCompat.Builder().build()
            }
        }
        mediaSessionConnector.setQueueNavigator(timelineQueueNavigator)
        mediaSessionConnector.setPlayer(musicPlayerManager.exoPlayer)
    }

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int = START_STICKY

    // goes into this method when application is destroyed
    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        playerNotificationManager.setPlayer(null)
        musicPlayerManager.exoPlayer.stop()
    }
}
