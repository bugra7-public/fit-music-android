package com.dobra.fitmusic.service

import android.app.Notification
import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.dobra.fitmusic.R
import com.dobra.fitmusic.manager.AutomaticSongChangeManager
import com.dobra.fitmusic.manager.RunningSessionManager
import com.dobra.fitmusic.model.RunningModeState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val FOREGROUND_ID = 1905

@AndroidEntryPoint
class RunningSessionService : Service() {
    @Inject
    lateinit var runningSessionManager: RunningSessionManager

    @Inject
    lateinit var automaticSongChangeManager: AutomaticSongChangeManager

    private val job = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + job)

    override fun onCreate() {
        super.onCreate()
        startForeground(FOREGROUND_ID, createNotification())
    }

    private fun createNotification(): Notification =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationCompat.Builder(this, getString(R.string.NotificationChannelId)).build()
        } else {
            Notification()
        }

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        coroutineScope.launch {
            runningSessionManager.currentSpeed.collectLatest {
                if (runningSessionManager.runningModeState.value == RunningModeState.RUNNING) {
                    automaticSongChangeManager.changeSongAccordingToSpeed(it)
                }
            }
            runningSessionManager.runningModeState.collectLatest {
                if (it == RunningModeState.FINISHED) {
                    automaticSongChangeManager.clearSpeedValues()
                }
            }
        }
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}
