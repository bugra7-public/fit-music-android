package com.dobra.fitmusic.model

enum class RunningModeState {
    NOT_STARTED, RUNNING, FINISHED, REJECTED_BY_LOCATION_PERMISSION,
    PAUSED // todo
}
