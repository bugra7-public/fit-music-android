package com.dobra.fitmusic.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RunningSession(
    @PrimaryKey(autoGenerate = false)
    val startTime: String,
    val duration: String,
    val totalDistanceInMeters: String,
    val averageSpeed: String,
)
