package com.dobra.fitmusic.model.relation

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.dobra.fitmusic.model.Playlist
import com.dobra.fitmusic.model.Song
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.exoplayer2.source.MediaSource

data class PlaylistWithSongs(
    @Embedded
    var playlist: Playlist,

    @Relation(
        parentColumn = "playlistId",
        entityColumn = "songUriId",
        associateBy = Junction(
            PlaylistSongCrossRef::class
        )
    )
    var songs: MutableList<Song>
) {
    constructor(
        name: String,
        concatenatingMediaSource: ConcatenatingMediaSource,
        songs: MutableList<Song>
    ) : this(Playlist(name, concatenatingMediaSource), songs)
}

fun PlaylistWithSongs.isEmpty(): Boolean = this.playlist.concatenatingMediaSource.size == 0

fun PlaylistWithSongs.size(): Int = this.playlist.concatenatingMediaSource.size

fun PlaylistWithSongs.mediaSourceAt(index: Int): MediaSource =
    this.playlist.concatenatingMediaSource.getMediaSource(index)

fun PlaylistWithSongs.name(): String = this.playlist.playlistName

fun PlaylistWithSongs.id(): Int = this.playlist.playlistId

fun PlaylistWithSongs.setId(id: Int) {
    this.playlist.playlistId = id
}

fun PlaylistWithSongs.concatenatingMediaSource(): ConcatenatingMediaSource =
    this.playlist.concatenatingMediaSource
