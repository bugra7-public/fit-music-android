package com.dobra.fitmusic.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RunningMoment(
    @PrimaryKey(autoGenerate = false)
    val time: Long,
    val latitude: Double,
    val longitude: Double,
    val runningSessionStartTime: String // this value acts as a foreign value, so don't delete it
)
