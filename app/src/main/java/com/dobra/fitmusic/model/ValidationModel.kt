package com.dobra.fitmusic.model

data class ValidationModel(
    val valid: Boolean,
    val errorMessageId: Int
)
