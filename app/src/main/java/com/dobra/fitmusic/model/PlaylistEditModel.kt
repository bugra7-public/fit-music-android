package com.dobra.fitmusic.model

data class PlaylistEditModel(
    var isEditOngoing: Boolean = false,
    var editingPlaylistIndex: Int = -1
)
