package com.dobra.fitmusic.model.relation

import androidx.room.Embedded
import androidx.room.Relation
import com.dobra.fitmusic.model.RunningMoment
import com.dobra.fitmusic.model.RunningSession

data class RunningSessionWithMoments(
    @Embedded val runningSession: RunningSession,
    @Relation(
        parentColumn = "startTime",
        entityColumn = "runningSessionStartTime"
    )
    val runningMoments: List<RunningMoment>
)
