package com.dobra.fitmusic.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.android.exoplayer2.source.ConcatenatingMediaSource

@Entity
data class Playlist(
    @PrimaryKey(autoGenerate = true)
    var playlistId: Int = 0,

    var playlistName: String,

    @Ignore
    var concatenatingMediaSource: ConcatenatingMediaSource,
) {
    constructor(playlistName: String) : this(playlistName, ConcatenatingMediaSource())

    constructor(playlistName: String, concatenatingMediaSource: ConcatenatingMediaSource) :
        this(0, playlistName, concatenatingMediaSource)
}
