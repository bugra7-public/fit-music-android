package com.dobra.fitmusic.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Song(
    @PrimaryKey(autoGenerate = false)
    val songUriId: Long,
    val songName: String,
    val duration: Long
)
