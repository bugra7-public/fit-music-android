package com.dobra.fitmusic.model.relation

import androidx.room.Entity

@Entity(primaryKeys = ["playlistId", "songUriId"])
data class PlaylistSongCrossRef(
    val playlistId: Int,
    val songUriId: Long
)
