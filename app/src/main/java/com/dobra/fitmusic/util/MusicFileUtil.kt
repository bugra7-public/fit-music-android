package com.dobra.fitmusic.util

import android.content.Context
import android.database.Cursor
import android.provider.MediaStore
import com.dobra.fitmusic.model.Song

private const val CURSOR_QUERY_SORT_ORDER_TYPE = "TITLE COLLATE NOCASE ASC"

class MusicFileUtil(val context: Context) {
    fun getAllSongsFromStorage(): MutableList<Song> {
        val cursor = createCursor()
        return cursor?.let {
            readMusicFiles(cursor)
        } ?: mutableListOf()
    }

    private fun createCursor(): Cursor? {
        return context.contentResolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
            null,
            MediaStore.Audio.Media.IS_MUSIC,
            null,
            CURSOR_QUERY_SORT_ORDER_TYPE
        )
    }

    private fun readMusicFiles(cursor: Cursor): MutableList<Song> {
        val songs: MutableList<Song> = mutableListOf()
        try {
            while (cursor.moveToNext()) {
                val mediaId = cursor.getColumnIndex(MediaStore.Audio.Media._ID)
                val mediaDuration = cursor.getString(
                    cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)
                )
                if (mediaId >= 0 && mediaDuration != null) {
                    songs.add(
                        Song(
                            cursor.getLong(mediaId),
                            cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE)),
                            mediaDuration.toLong()
                        )
                    )
                }
            }
        } catch (ignored: Exception) {
        } finally {
            cursor.close()
        }
        return songs
    }
}
