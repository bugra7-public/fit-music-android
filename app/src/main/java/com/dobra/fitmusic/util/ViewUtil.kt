package com.dobra.fitmusic.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.FragmentManager
import com.dobra.fitmusic.R
import com.dobra.fitmusic.common.CONFIRMATION_DIALOG_TAG
import com.dobra.fitmusic.common.INFO_DIALOG_TAG
import com.dobra.fitmusic.dialog.ConfirmationDialog
import com.dobra.fitmusic.dialog.IConfirmationDialogListener
import com.dobra.fitmusic.dialog.InfoDialog
import com.google.android.material.textview.MaterialTextView

class ViewUtil {
    var playlistDropdownSelectedPosition: Int = 0

    fun changeActivity(context: Context, newActivityClass: Class<out Activity>) {
        val intent = Intent(context, newActivityClass)
        context.startActivity(intent)
        if (context is Activity) {
            context.finish()
        }
    }

    fun createDialogTitleView(inflater: LayoutInflater, titleText: String): View {
        val titleView = inflater.inflate(R.layout.dialog_title, null)

        val tvTitle = titleView.findViewById<MaterialTextView>(R.id.tvTitle)
        tvTitle.text = titleText

        return titleView
    }

    fun changeTextColor(text: CharSequence, color: Int): SpannableString {
        val spannableString = SpannableString(text)
        spannableString.setSpan(ForegroundColorSpan(color), 0, spannableString.length, 0)

        return spannableString
    }

    fun createInfoDialog(titleText: String, bodyText: String, fragmentManager: FragmentManager) {
        val infoDialog = InfoDialog(titleText, bodyText)
        infoDialog.show(fragmentManager, INFO_DIALOG_TAG)
    }

    fun createConfirmationDialog(
        titleText: String,
        bodyText: String,
        index: Int,
        confirmationListener: IConfirmationDialogListener,
        fragmentManager: FragmentManager
    ) {
        val confirmationDialog = ConfirmationDialog(
            titleText,
            bodyText,
            index,
            confirmationListener
        )
        confirmationDialog.show(fragmentManager, CONFIRMATION_DIALOG_TAG)
    }

    fun checkPermissionsGranted(
        permissionsToBeGrantedAtRuntime: Array<String>,
        permissions: Array<String>,
        grantResults: IntArray
    ): Boolean {
        val permissionsNeeded = permissionsToBeGrantedAtRuntime.size
        var permissionsGranted = 0
        for (i in permissions.indices) {
            if (permissions[i] == permissionsToBeGrantedAtRuntime[i] && grantResults[i] == 0) {
                permissionsGranted++
            }

            if (permissionsGranted == permissionsNeeded) {
                return true
            }
        }
        return false
    }
}
