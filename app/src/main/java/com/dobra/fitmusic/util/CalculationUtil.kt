package com.dobra.fitmusic.util

import com.dobra.fitmusic.common.ZERO
import com.dobra.fitmusic.extension.roundTo2FractionalDigits
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.time.Duration.Companion.milliseconds

@Suppress("ReturnCount", "MagicNumber")
class CalculationUtil {

    fun speed(distanceInMeters: Float, oldTime: Long?, newTime: Long): Float {
        if (oldTime == null) return ZERO
        val secondsPassed = (newTime - oldTime) / 1000F
        val hoursPassed: Float = secondsPassed / 3600F
        val distanceInKilometers = distanceInMeters / 1000

        if (distanceInKilometers == ZERO || hoursPassed == ZERO) return ZERO

        val speed = distanceInKilometers / hoursPassed

        return Float.roundTo2FractionalDigits(speed)
    }

    fun distance(lat1: Double?, lng1: Double?, lat2: Double, lng2: Double): Float {
        if (lat1 == null || lng1 == null) return ZERO
        val lat = (lat1 - lat2) * distPerLatitude(lat1)
        val lon = (lng1 - lng2) * distPerLongitude(lat1)
        return sqrt(lat * lat + lon * lon).toFloat()
    }

    private fun distPerLongitude(lat: Double): Double {
        return lat.pow(4) * 0.0003121092 + 0.0101182384 * lat.pow(3) -
            17.2385140059 * lat * lat + 5.5485277537 * lat + 111301.967182595
    }

    private fun distPerLatitude(lat: Double): Double {
        return lat.pow(4) * -0.000000487305676 - 0.0033668574 * lat.pow(3) +
            0.4601181791 * lat * lat - 1.4558127346 * lat + 110579.25662316
    }

    fun calculateDuration(startTime: Long, endTime: Long): Pair<String, String> {
        val timeDifferenceInMillis = (endTime - startTime).milliseconds
        val minutes = timeDifferenceInMillis.inWholeMinutes
        val seconds = timeDifferenceInMillis.inWholeSeconds - minutes * 60

        return Pair(minutes.toString(), seconds.toString())
    }

    fun createFormattedDateString(dateLong: Long): String {
        val date = Date(dateLong)
        val calendar = Calendar.getInstance()
        calendar.time = date

        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy  HH:mm  EEEE")
        simpleDateFormat.timeZone = TimeZone.getDefault()

        return simpleDateFormat.format(calendar.time)
    }

    fun convertMillisToDurationText(durationInMillis: Long): String {
        return try {
            val durationArray = durationInMillis.div(60000F).toString().split(".")
            var seconds: Int = (durationArray[1].take(3).toInt()) * 60 / 1000

            if (seconds == 60) seconds = 59

            var secondsPart: String = seconds.toString()

            if (seconds < 10) secondsPart = "0$secondsPart"

            durationArray[0] + ":" + secondsPart
        } catch (ignored: Exception) {
            ""
        }
    }
}
