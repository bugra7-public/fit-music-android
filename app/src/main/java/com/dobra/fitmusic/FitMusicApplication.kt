package com.dobra.fitmusic

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FitMusicApplication : Application()
