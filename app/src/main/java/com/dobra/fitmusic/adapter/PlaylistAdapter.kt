package com.dobra.fitmusic.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dobra.fitmusic.R
import com.dobra.fitmusic.databinding.RowPlaylistBinding
import com.dobra.fitmusic.model.relation.PlaylistWithSongs
import com.dobra.fitmusic.model.relation.name
import com.dobra.fitmusic.model.relation.size

class PlaylistAdapter(
    private var playlistWithSongs: List<PlaylistWithSongs>,
    private var playlistAdapterListener: IPlaylistAdapterListener
) : RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder>() {
    inner class PlaylistViewHolder(val bind: RowPlaylistBinding) :
        RecyclerView.ViewHolder(bind.root)

    lateinit var context: Context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PlaylistViewHolder {
        context = parent.context

        return PlaylistViewHolder(
            RowPlaylistBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: PlaylistViewHolder, position: Int) {
        val playlist = playlistWithSongs[position]
        val playlistSize = playlist.size()

        holder.bind.tvPlaylistName.text = playlist.name()
        holder.bind.tvNumberOfSongs.text =
            if (playlistSize < 2) {
                context.getString(R.string.song, playlistSize)
            } else {
                context.getString(R.string.songs, playlistSize)
            }

        holder.itemView.setOnClickListener {
            playlistAdapterListener.showPlaylistMenu(position, playlist.name(), holder.itemView)
        }
    }

    override fun getItemCount(): Int = playlistWithSongs.size
}

interface IPlaylistAdapterListener {
    fun showPlaylistMenu(index: Int, playlistName: String, view: View)
}
