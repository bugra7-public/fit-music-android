package com.dobra.fitmusic.adapter

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import com.dobra.fitmusic.R
import com.dobra.fitmusic.activity.MainActivity
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerNotificationManager

private const val PENDING_INTENT_REQUEST_CODE = 0

class PlayerNotificationAdapter(
    private val context: Context
) : PlayerNotificationManager.MediaDescriptionAdapter {

    override fun getCurrentContentTitle(player: Player): CharSequence {
        if (player.currentMediaItem?.mediaMetadata?.title != null) {
            return player.currentMediaItem!!.mediaMetadata.title!!
        }
        return context.getString(R.string.SongNotFound)
    }

    override fun createCurrentContentIntent(player: Player): PendingIntent? {
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        }
        val flags = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
        } else {
            PendingIntent.FLAG_UPDATE_CURRENT
        }
        return PendingIntent.getActivity(context, PENDING_INTENT_REQUEST_CODE, intent, flags)
    }

    override fun getCurrentContentText(player: Player): CharSequence? =
        player.currentMediaItem?.mediaMetadata?.artist

    override fun getCurrentLargeIcon(
        player: Player,
        callback: PlayerNotificationManager.BitmapCallback
    ): Bitmap? = null
}
