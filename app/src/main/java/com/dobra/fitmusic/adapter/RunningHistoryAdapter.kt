package com.dobra.fitmusic.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dobra.fitmusic.R
import com.dobra.fitmusic.databinding.RowRunningHistoryBinding
import com.dobra.fitmusic.model.RunningMoment
import com.dobra.fitmusic.model.relation.RunningSessionWithMoments

class RunningHistoryAdapter(
    private var runningSessionWithMomentsList: List<RunningSessionWithMoments>,
    private var runningHistoryAdapterListener: IRunningHistoryAdapterListener
) : RecyclerView.Adapter<RunningHistoryAdapter.RunningHistoryViewHolder>() {
    inner class RunningHistoryViewHolder(val bind: RowRunningHistoryBinding) :
        RecyclerView.ViewHolder(bind.root)

    lateinit var context: Context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RunningHistoryViewHolder {
        context = parent.context
        return RunningHistoryViewHolder(
            RowRunningHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: RunningHistoryViewHolder, position: Int) {
        val runningSessionWithMoments = runningSessionWithMomentsList[position]
        val runningSession = runningSessionWithMoments.runningSession
        holder.bind.tvIndex.text = (position + 1).toString()
        holder.bind.tvVarStartDate.text = runningSession.startTime
        holder.bind.tvVarDuration.text = runningSession.duration
        holder.bind.tvVarDistance.text =
            context.getString(R.string.DistanceWithUnit, runningSession.totalDistanceInMeters)
        holder.bind.tvVarAverageSpeed.text =
            context.getString(R.string.SpeedWithUnit, runningSession.averageSpeed)
        holder.bind.root.setOnClickListener {
            runningHistoryAdapterListener.onRunningHistoryItemSelected(runningSessionWithMoments.runningMoments)
        }
    }

    override fun getItemCount(): Int = runningSessionWithMomentsList.size
}

interface IRunningHistoryAdapterListener {
    fun onRunningHistoryItemSelected(runningMoments: List<RunningMoment>)
}

