package com.dobra.fitmusic.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.dobra.fitmusic.R
import com.dobra.fitmusic.databinding.RowSongBinding
import com.dobra.fitmusic.extension.durationText
import com.dobra.fitmusic.extension.name
import com.google.android.exoplayer2.source.ConcatenatingMediaSource

class SongAdapter(
    private var mediaSources: ConcatenatingMediaSource,
    private var songAdapterListener: SongAdapterListener,
    private var selectedRowPosition: Int
) : RecyclerView.Adapter<SongAdapter.SongViewHolder>() {
    inner class SongViewHolder(val bind: RowSongBinding) : RecyclerView.ViewHolder(bind.root)

    lateinit var context: Context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SongViewHolder {
        context = parent.context
        return SongViewHolder(
            RowSongBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int = mediaSources.size

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        val mediaSource = mediaSources.getMediaSource(position)
        holder.bind.tvSongName.text = mediaSource.name()
        holder.bind.tvSongDuration.text = mediaSource.durationText()
        holder.itemView.setOnClickListener {
            songAdapterListener.playSong(position, mediaSource.mediaItem.localConfiguration?.uri)
            val oldPosition = selectedRowPosition
            selectRow(holder.bind, position)
            notifyItemChanged(oldPosition)
        }
        if (selectedRowPosition == position) {
            selectRow(holder.bind, position)
        } else {
            unselectRow(holder.bind)
        }
    }

    fun updatePlayingSongColor(newPlayingSongIndex: Int) {
        val oldPlayingSongIndex = selectedRowPosition
        selectedRowPosition = newPlayingSongIndex
        notifyItemChanged(oldPlayingSongIndex)
        notifyItemChanged(newPlayingSongIndex)
    }

    private fun selectRow(rowBind: RowSongBinding, position: Int) {
        rowBind.tvSongName.setTextColor(getColor(R.color.fit_turquoise))
        rowBind.tvSongDuration.setTextColor(getColor(R.color.fit_turquoise))
        selectedRowPosition = position
    }

    private fun unselectRow(rowBind: RowSongBinding) {
        rowBind.tvSongName.setTextColor(getColor(R.color.fit_white))
        rowBind.tvSongDuration.setTextColor(getColor(R.color.fit_white))
    }

    private fun getColor(colorResId: Int) = ContextCompat.getColor(context, colorResId)
}

interface SongAdapterListener {
    fun playSong(adapterIndex: Int, songUri: Uri?)
}
