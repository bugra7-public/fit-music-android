package com.dobra.fitmusic.vm

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import com.dobra.fitmusic.dao.IRunningSessionDao
import com.dobra.fitmusic.model.RunningMoment
import com.yandex.mapkit.geometry.BoundingBox
import com.yandex.mapkit.geometry.Point
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

private const val MAP_EXTENDER_CONST = 0.015

@HiltViewModel
class RunningHistoryAndMapSharedViewModel @Inject constructor(
    runningSessionDao: IRunningSessionDao
) : ViewModel(), LifecycleObserver {
    var runningSessionWithMomentsList = runningSessionDao.getRunningSessionWithMoments()

    lateinit var selectedRunningSessionMoments: List<RunningMoment>

    fun createPolylinePoints(): MutableList<Point> {
        val polylinePoints = mutableListOf<Point>()
        for (runningMoment in selectedRunningSessionMoments) {
            val polylinePoint = Point(runningMoment.latitude, runningMoment.longitude)
            polylinePoints.add(polylinePoint)
        }
        return polylinePoints
    }

    fun createBoundingBox(points: List<Point>): BoundingBox {
        var topMostPoint = points[0].latitude
        var bottomMostPoint = points[0].latitude
        var leftMostPoint = points[0].longitude
        var rightMostPoint = points[0].longitude

        for (point in points) {
            if (point.latitude > topMostPoint) {
                topMostPoint = point.latitude
            } else if (point.latitude < bottomMostPoint) {
                bottomMostPoint = point.latitude
            }

            if (point.longitude > rightMostPoint) {
                rightMostPoint = point.longitude
            } else if (point.longitude < leftMostPoint) {
                leftMostPoint = point.longitude
            }
        }

        return BoundingBox(
            Point(bottomMostPoint - MAP_EXTENDER_CONST, leftMostPoint - MAP_EXTENDER_CONST),
            Point(topMostPoint + MAP_EXTENDER_CONST, rightMostPoint + MAP_EXTENDER_CONST)
        )
    }
}
