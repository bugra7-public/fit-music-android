package com.dobra.fitmusic.vm

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dobra.fitmusic.common.DataStoreKeys.KEY_CURRENT_PLAYLIST_INDEX
import com.dobra.fitmusic.common.DataStoreKeys.KEY_CURRENT_SONG_INDEX
import com.dobra.fitmusic.common.DataStoreKeys.KEY_CURRENT_SONG_TIME_POSITION_IN_MS
import com.dobra.fitmusic.common.DataStoreKeys.KEY_HIGH_SPEED_THRESHOLD
import com.dobra.fitmusic.common.DataStoreKeys.KEY_LOW_SPEED_THRESHOLD
import com.dobra.fitmusic.common.DataStoreKeys.KEY_REPEAT_PRESSED
import com.dobra.fitmusic.common.DataStoreKeys.KEY_SHUFFLE_PRESSED
import com.dobra.fitmusic.common.PlaylistType
import com.dobra.fitmusic.common.highSpeedThreshold
import com.dobra.fitmusic.common.lowSpeedThreshold
import com.dobra.fitmusic.dao.IPlaylistDao
import com.dobra.fitmusic.dao.IRunningSessionDao
import com.dobra.fitmusic.manager.AudioDataManager
import com.dobra.fitmusic.manager.DataStoreManager
import com.dobra.fitmusic.manager.MusicPlayerManager
import com.dobra.fitmusic.manager.RunningSessionManager
import com.dobra.fitmusic.model.Song
import com.dobra.fitmusic.model.relation.PlaylistWithSongs
import com.dobra.fitmusic.model.relation.RunningSessionWithMoments
import com.dobra.fitmusic.model.relation.concatenatingMediaSource
import com.dobra.fitmusic.util.CalculationUtil
import com.dobra.fitmusic.util.MusicFileUtil
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaMetadata
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

private const val SLIDER_UPDATE_FREQUENCY = 500L

@Suppress("LongParameterList")
@HiltViewModel
class MainActivityViewModel @Inject constructor(
    private val musicPlayerManager: MusicPlayerManager,
    val audioDataManager: AudioDataManager,
    private val playlistDao: IPlaylistDao,
    private val runningSessionDao: IRunningSessionDao,
    private val calculationUtil: CalculationUtil,
    private val dataStoreManager: DataStoreManager,
    private val musicFileUtil: MusicFileUtil,
    val runningSessionManager: RunningSessionManager
) : ViewModel(), LifecycleObserver {
    val isMusicPlaying: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val currentSongName: MutableLiveData<String> = MutableLiveData()
    val currentSongDuration: MutableStateFlow<Long> = MutableStateFlow(1)
    val shuffleEnabled: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val repeatEnabled: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val audioCurrentPositionFlow = flow {
        while (true) {
            delay(SLIDER_UPDATE_FREQUENCY)
            emit(musicPlayerManager.exoPlayer.currentPosition.toFloat())
        }
    }

    init {
        musicPlayerManager.exoPlayer.addListener(object : Player.Listener {
            override fun onMediaMetadataChanged(mediaMetadata: MediaMetadata) {
                currentSongName.value = mediaMetadata.title.toString()
            }

            override fun onIsPlayingChanged(isPlaying: Boolean) {
                isMusicPlaying.value = isPlaying
            }

            override fun onPlaybackStateChanged(playbackState: Int) {
                if (playbackState == ExoPlayer.STATE_READY) {
                    currentSongDuration.value = musicPlayerManager.exoPlayer.duration
                }
            }
        })
        runBlocking {
            audioDataManager.allPlaylistsWithSongs =
                playlistDao.getPlaylistsWithSongs().toMutableList()
                    .onEach { it.songs.sortBy { song -> song.songName.lowercase() } }
            setConcatenatingMediaSourcesOfPlaylists()
            restoreApplicationState()
        }
    }

    fun getAllSongsFromStorage(): MutableList<Song> = musicFileUtil.getAllSongsFromStorage()

    fun createPlaylists(
        allSongs: MutableList<Song>,
        allSongsName: String,
        slowSongsName: String,
        fastSongsName: String
    ) {
        val concatenatingMediaSource = audioDataManager.createConcatenatingMediaSource(allSongs)
        audioDataManager.allPlaylistsWithSongs = arrayListOf(
            PlaylistWithSongs(allSongsName, concatenatingMediaSource, allSongs),
            PlaylistWithSongs(slowSongsName, ConcatenatingMediaSource(), arrayListOf()),
            PlaylistWithSongs(fastSongsName, ConcatenatingMediaSource(), arrayListOf())
        )
    }

    fun processPlayButtonClick() {
        if (musicPlayerManager.isPlaying()) {
            musicPlayerManager.pauseSong()
        } else {
            musicPlayerManager.continueSong()
        }
    }

    fun processNextButtonClick() = musicPlayerManager.playNextSong()

    fun processPreviousButtonClick() = musicPlayerManager.playPreviousSong()

    fun processShuffleButtonClick() {
        shuffleEnabled.value = !shuffleEnabled.value
        musicPlayerManager.exoPlayer.shuffleModeEnabled = shuffleEnabled.value
        if (shuffleEnabled.value && repeatEnabled.value) {
            repeatEnabled.value = false
            musicPlayerManager.exoPlayer.repeatMode = Player.REPEAT_MODE_ALL
        }
    }

    fun processRepeatButtonClick() {
        repeatEnabled.value = !repeatEnabled.value
        if (repeatEnabled.value && shuffleEnabled.value) {
            shuffleEnabled.value = false
            musicPlayerManager.exoPlayer.shuffleModeEnabled = false
        }

        if (repeatEnabled.value) {
            musicPlayerManager.exoPlayer.repeatMode = Player.REPEAT_MODE_ONE
        } else {
            musicPlayerManager.exoPlayer.repeatMode = Player.REPEAT_MODE_ALL
        }
    }

    fun stopPlayWhenReadyFeatureOfMusicPlayer() {
        musicPlayerManager.exoPlayer.playWhenReady = false
    }

    fun setConcatenatingMediaSourcesOfPlaylists() =
        audioDataManager.setConcatenatingMediaSourcesOfPlaylists()

    fun savePlaylistsToLocalDatabase() {
        viewModelScope.launch {
            audioDataManager.allPlaylistsWithSongs.forEach { playlistWithSongs ->
                playlistDao.savePlaylistToLocalDatabase(playlistWithSongs, true)
            }
        }
    }

    fun seekSongTo(position: Long) = musicPlayerManager.exoPlayer.seekTo(position)

    fun formatSongDuration(durationInMilliseconds: Float): String =
        calculationUtil.convertMillisToDurationText(durationInMilliseconds.toLong())

    fun setInitialPlaylist() {
        audioDataManager.currentPlaylistWithSongs.value = audioDataManager.allPlaylistsWithSongs[0]
    }

    fun saveApplicationState() {
        viewModelScope.launch {
            dataStoreManager.saveBoolean(KEY_REPEAT_PRESSED, repeatEnabled.value)
            dataStoreManager.saveBoolean(KEY_SHUFFLE_PRESSED, shuffleEnabled.value)
            dataStoreManager.saveInteger(
                KEY_CURRENT_PLAYLIST_INDEX,
                audioDataManager.indexOfCurrentPlaylist()
            )
            dataStoreManager.saveInteger(
                KEY_CURRENT_SONG_INDEX,
                musicPlayerManager.exoPlayer.currentMediaItemIndex
            )
            dataStoreManager.saveLong(
                KEY_CURRENT_SONG_TIME_POSITION_IN_MS,
                musicPlayerManager.exoPlayer.currentPosition
            )
        }
    }

    fun startRunningSession() {
        if (audioDataManager.currentPlaylistType() == PlaylistType.OTHER) {
            audioDataManager.currentPlaylistWithSongs.value = audioDataManager.slowPlaylist()
        }
        musicPlayerManager.setMusicPlayer(
            audioDataManager.currentPlaylistWithSongs.value!!.concatenatingMediaSource(),
            0,
            0
        )
        if (!musicPlayerManager.isPlaying()) {
            musicPlayerManager.continueSong()
        }
        runningSessionManager.startRunningSession()
    }

    fun endRunningSession() =
        saveRunningSessionToLocalDatabase(runningSessionManager.endRunningSession())

    private fun saveRunningSessionToLocalDatabase(runningSessionWithMoments: RunningSessionWithMoments?) {
        if (runningSessionWithMoments != null) {
            viewModelScope.launch {
                runningSessionDao.saveRunningSessionToLocalDatabase(runningSessionWithMoments)
            }
        }
    }

    private suspend fun restoreApplicationState() {
        val repeatOn = dataStoreManager.readBoolean(KEY_REPEAT_PRESSED)
        val shuffleOn = dataStoreManager.readBoolean(KEY_SHUFFLE_PRESSED)
        val latestPlaylistIndex = dataStoreManager.readInteger(KEY_CURRENT_PLAYLIST_INDEX) ?: 0
        val latestSongIndex = dataStoreManager.readInteger(KEY_CURRENT_SONG_INDEX) ?: 0
        val songTimePositionInMs =
            dataStoreManager.readLong(KEY_CURRENT_SONG_TIME_POSITION_IN_MS) ?: 0
        if (repeatOn == true) {
            processRepeatButtonClick()
        }
        if (shuffleOn == true) {
            processShuffleButtonClick()
        }
        dataStoreManager.readFloat(KEY_LOW_SPEED_THRESHOLD)?.let {
            lowSpeedThreshold = it
        }
        dataStoreManager.readFloat(KEY_HIGH_SPEED_THRESHOLD)?.let {
            highSpeedThreshold = it
        }
        if (audioDataManager.allPlaylistsWithSongs.size > 0) {
            audioDataManager.currentPlaylistWithSongs.value =
                audioDataManager.allPlaylistsWithSongs[latestPlaylistIndex]
        }

        audioDataManager.currentPlaylistWithSongs.value?.let {
            try {
                musicPlayerManager.setMusicPlayer(
                    it.concatenatingMediaSource(),
                    latestSongIndex,
                    songTimePositionInMs
                )
            } catch (ignored: Exception) {
            }
        }
    }
}
