package com.dobra.fitmusic.vm

import android.util.SparseBooleanArray
import androidx.core.util.forEach
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dobra.fitmusic.R
import com.dobra.fitmusic.common.FAST_PLAYLIST_INDEX
import com.dobra.fitmusic.dao.IPlaylistDao
import com.dobra.fitmusic.manager.AudioDataManager
import com.dobra.fitmusic.model.PlaylistEditModel
import com.dobra.fitmusic.model.Song
import com.dobra.fitmusic.model.ValidationModel
import com.dobra.fitmusic.model.relation.PlaylistSongCrossRef
import com.dobra.fitmusic.model.relation.PlaylistWithSongs
import com.dobra.fitmusic.model.relation.concatenatingMediaSource
import com.dobra.fitmusic.model.relation.id
import com.dobra.fitmusic.model.relation.name
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

// if you change this, change R.string.PlaylistNameTooLong also
const val PLAYLIST_NAME_CHAR_LIMIT = 15

@Suppress("TooManyFunctions", "ReturnCount")
@HiltViewModel
class PlaylistFragmentViewModel @Inject constructor(
    private val audioDataManager: AudioDataManager,
    private val fitMusicDao: IPlaylistDao
) : ViewModel(), LifecycleObserver {
    lateinit var newPlaylistName: String
    private var playlistEditModel = PlaylistEditModel()

    fun allPlaylists(): List<PlaylistWithSongs> = audioDataManager.allPlaylistsWithSongs

    fun isPlaylistNameValid(playlistName: String?): ValidationModel {
        if (playlistName == null || playlistName.isEmpty()) {
            return ValidationModel(false, R.string.PlaylistNameCannotBeEmpty)
        }
        if (playlistName.length > PLAYLIST_NAME_CHAR_LIMIT) {
            return ValidationModel(false, R.string.PlaylistNameTooLong)
        }
        if (audioDataManager.allPlaylistsWithSongs.any { it.name().equals(playlistName, true) }) {
            return ValidationModel(false, R.string.PlaylistNameAlreadyExists)
        }
        return ValidationModel(true, 0)
    }

    fun createNewPlaylist(newPlaylistName: String, songSelectionIndexes: SparseBooleanArray) {
        val selectedSongs = arrayListOf<Song>()

        songSelectionIndexes.forEach { index, isSelected ->
            if (isSelected) selectedSongs.add(audioDataManager.allPlaylistsWithSongs[0].songs[index])
        }

        val playlistWithSongs = PlaylistWithSongs(
            newPlaylistName,
            audioDataManager.createConcatenatingMediaSource(selectedSongs),
            selectedSongs
        )

        audioDataManager.allPlaylistsWithSongs.add(playlistWithSongs)

        viewModelScope.launch {
            fitMusicDao.savePlaylistToLocalDatabase(playlistWithSongs, false)
        }
    }

    fun changeActivePlaylist(index: Int) {
        audioDataManager.currentPlaylistWithSongs.value =
            audioDataManager.allPlaylistsWithSongs[index]
    }

    fun isPlaylistEditOngoing(): Boolean = playlistEditModel.isEditOngoing

    fun setPlaylistEditOngoing(isOngoing: Boolean, playlistIndex: Int) {
        playlistEditModel.isEditOngoing = isOngoing
        playlistEditModel.editingPlaylistIndex = playlistIndex
    }

    fun renamePlaylist(playlistNameInput: String) {
        val playlist = audioDataManager
            .allPlaylistsWithSongs[playlistEditModel.editingPlaylistIndex].playlist

        playlist.playlistName = playlistNameInput

        viewModelScope.launch {
            fitMusicDao.updatePlaylist(playlist)
        }
    }

    fun editingPlaylistIndex(): Int = playlistEditModel.editingPlaylistIndex

    fun deletePlaylist(index: Int) {
        viewModelScope.launch {
            fitMusicDao.deletePlaylistFromLocalDatabase(audioDataManager.allPlaylistsWithSongs[index])
        }
        audioDataManager.allPlaylistsWithSongs.removeAt(index)
    }

    fun updatePlaylistSongs(songSelectionIndexes: SparseBooleanArray) {
        val editingPlaylist = audioDataManager.allPlaylistsWithSongs[editingPlaylistIndex()]
        val allSongsPlaylist = audioDataManager.allPlaylistsWithSongs[0].songs
        var placementIndex = 0

        songSelectionIndexes.forEach { index, isSelected ->
            val songId = allSongsPlaylist[index].songUriId

            val songInPlaylist = editingPlaylist.songs.firstOrNull {
                it.songUriId == songId
            }
            if (songInPlaylist != null) {
                placementIndex++
            }
            if (isSelected && songInPlaylist == null) {
                viewModelScope.launch {
                    fitMusicDao.insertPlaylistSongCrossRef(
                        PlaylistSongCrossRef(
                            editingPlaylist.id(),
                            songId
                        )
                    )
                }
                editingPlaylist.concatenatingMediaSource().addMediaSource(
                    placementIndex,
                    audioDataManager.createSingleMediaSource(allSongsPlaylist[index])
                )
                editingPlaylist.songs.add(placementIndex, allSongsPlaylist[index])
                placementIndex++
            } else if (!isSelected && songInPlaylist != null) {
                viewModelScope.launch {
                    fitMusicDao.deletePlaylistSongCrossRef(
                        PlaylistSongCrossRef(
                            editingPlaylist.id(),
                            songId
                        )
                    )
                }
                editingPlaylist.concatenatingMediaSource()
                    .removeMediaSource(editingPlaylist.songs.indexOf(songInPlaylist))
                editingPlaylist.songs.remove(songInPlaylist)
                placementIndex--
            }
        }
    }

    fun getAllSongs(): PlaylistWithSongs = audioDataManager.allPlaylistsWithSongs[0]

    fun getEditingPlaylistSongs(): List<Song>? {
        if (isPlaylistEditOngoing()) {
            return audioDataManager.allPlaylistsWithSongs[editingPlaylistIndex()].songs
        }
        return null
    }

    fun isRenameAllowed(index: Int): ValidationModel {
        if (index > -1 && index <= FAST_PLAYLIST_INDEX) {
            return ValidationModel(false, R.string.First3PlaylistCannotBeRenamedOrDeleted)
        } else if (index < 0 || index >= audioDataManager.allPlaylistsWithSongs.size) {
            return ValidationModel(false, R.string.InvalidIndex)
        }
        return ValidationModel(true, 0)
    }

    fun isChangeSongsAllowed(index: Int): ValidationModel {
        if (index == 0) {
            return ValidationModel(false, R.string.FirstPlaylistSongsCannotBeChanged)
        } else if (audioDataManager.currentPlaylistWithSongs.value!!.id() ==
            audioDataManager.allPlaylistsWithSongs[index].id()
        ) {
            return ValidationModel(false, R.string.ActivePlaylistSongsCannotBeChanged)
        }
        return ValidationModel(true, 0)
    }

    fun isDeleteAllowed(index: Int): ValidationModel {
        if (index > -1 && index <= FAST_PLAYLIST_INDEX) {
            return ValidationModel(false, R.string.First3PlaylistCannotBeRenamedOrDeleted)
        } else if (audioDataManager.currentPlaylistWithSongs.value!!.id() ==
            audioDataManager.allPlaylistsWithSongs[index].id()
        ) {
            return ValidationModel(false, R.string.ActivePlaylistCannotBeDeleted)
        }
        return ValidationModel(true, 0)
    }
}
