package com.dobra.fitmusic.vm

import android.Manifest
import android.os.Build
import androidx.lifecycle.ViewModel

class PermissionActivityViewModel : ViewModel() {
    var permissionsToBeGrantedAtRuntime = arrayOf(getAudioReadPermission())

    private fun getAudioReadPermission(): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            Manifest.permission.READ_MEDIA_AUDIO
        } else {
            Manifest.permission.READ_EXTERNAL_STORAGE
        }
    }
}
