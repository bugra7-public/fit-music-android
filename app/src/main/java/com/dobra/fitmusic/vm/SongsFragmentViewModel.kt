package com.dobra.fitmusic.vm

import android.net.Uri
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import com.dobra.fitmusic.R
import com.dobra.fitmusic.adapter.SongAdapterListener
import com.dobra.fitmusic.extension.createUri
import com.dobra.fitmusic.manager.AudioDataManager
import com.dobra.fitmusic.manager.IMusicFileSyncResultListener
import com.dobra.fitmusic.manager.MusicPlayerManager
import com.dobra.fitmusic.manager.SongSyncManager
import com.dobra.fitmusic.model.ValidationModel
import com.dobra.fitmusic.model.relation.PlaylistWithSongs
import com.dobra.fitmusic.model.relation.concatenatingMediaSource
import com.dobra.fitmusic.model.relation.isEmpty
import com.dobra.fitmusic.model.relation.name
import com.dobra.fitmusic.model.relation.size
import com.google.android.exoplayer2.MediaMetadata
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class SongsFragmentViewModel @Inject constructor(
    private val songSyncManager: SongSyncManager,
    val audioDataManager: AudioDataManager,
    val musicPlayerManager: MusicPlayerManager
) : ViewModel(), LifecycleObserver, SongAdapterListener {
    private var shouldChangePlaylist = true
    private var searchOngoing = false
    private var isFirstTimeForPreparingPlaylist = true
    val currentSongIndex = MutableStateFlow(musicPlayerManager.exoPlayer.currentMediaItemIndex)

    init {
        musicPlayerManager.exoPlayer.addListener(object : Player.Listener {
            override fun onMediaMetadataChanged(mediaMetadata: MediaMetadata) {
                currentSongIndex.value = musicPlayerManager.exoPlayer.currentMediaItemIndex
            }
        })
    }

    override fun playSong(adapterIndex: Int, songUri: Uri?) {
        val songList = audioDataManager.currentPlaylistWithSongs.value!!.songs
        val index = if (!searchOngoing) {
            adapterIndex
        } else {
            songList.indexOf(
                songList.find { songUri == Long.createUri(it.songUriId) }
            )
        }
        musicPlayerManager.startSong(
            audioDataManager.currentPlaylistWithSongs.value!!.concatenatingMediaSource(),
            index,
            true
        )
    }

    fun currentPlaylist(): PlaylistWithSongs = audioDataManager.currentPlaylistWithSongs.value!!

    fun changePlaylist(position: Int) {
        if (position < 0) return
        if (shouldChangePlaylist) {
            shouldChangePlaylist = false
            audioDataManager.currentPlaylistWithSongs.value =
                audioDataManager.allPlaylistsWithSongs[position]
        } else {
            shouldChangePlaylist = true
        }
    }

    fun prepareNewPlaylist(runningModeActive: Boolean) {
        if (isFirstTimeForPreparingPlaylist) {
            // this method should not be called in the first time the SongsFragment is created
            isFirstTimeForPreparingPlaylist = false
            return
        }
        if (!runningModeActive) {
            musicPlayerManager.pauseSong()
            musicPlayerManager.setMusicPlayer(
                audioDataManager.currentPlaylistWithSongs.value!!.concatenatingMediaSource(),
                0,
                0
            )
        }
    }

    fun isFirstPlaylistEmpty(): Boolean = audioDataManager.allPlaylistsWithSongs[0].isEmpty()

    fun getPlaylistNames(): List<String> {
        val playlistNames: MutableList<String> = mutableListOf()

        for (playlist in audioDataManager.allPlaylistsWithSongs) {
            playlistNames.add(playlist.name())
        }
        return playlistNames
    }

    fun isRunningAllowed(): ValidationModel {
        if (audioDataManager.allPlaylistsWithSongs[1].size() > 0 &&
            audioDataManager.allPlaylistsWithSongs[2].size() > 0
        ) {
            return ValidationModel(true, 0)
        }
        return ValidationModel(false, R.string.TrainingStartError)
    }

    fun filterSongs(searchedText: String): ConcatenatingMediaSource {
        val filteredSongList = currentPlaylist().songs.filter {
            it.songName.contains(searchedText, true)
        }
        val filteredConcatenatingMediaSource =
            audioDataManager.createConcatenatingMediaSource(filteredSongList)
        searchOngoing = true
        return filteredConcatenatingMediaSource
    }

    fun resetSearch() {
        searchOngoing = false
    }

    fun syncSongsWithLocalStorage(musicFileSyncResultListener: IMusicFileSyncResultListener) =
        songSyncManager.syncSongsWithLocalStorage(musicFileSyncResultListener)
}
