package com.dobra.fitmusic.vm

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import com.dobra.fitmusic.R
import com.dobra.fitmusic.common.DataStoreKeys
import com.dobra.fitmusic.common.highSpeedThreshold
import com.dobra.fitmusic.common.lowSpeedThreshold
import com.dobra.fitmusic.manager.DataStoreManager
import com.dobra.fitmusic.manager.IMusicFileSyncResultListener
import com.dobra.fitmusic.manager.SongSyncManager
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

private const val THRESHOLD_MAX_VALUE = 999

@HiltViewModel
class ProfileFragmentViewModel @Inject constructor(
    private val dataStoreManager: DataStoreManager,
    private val songSyncManager: SongSyncManager
) : ViewModel(), LifecycleObserver {

    fun syncSongsWithLocalStorage(musicFileSyncResultListener: IMusicFileSyncResultListener) =
        songSyncManager.syncSongsWithLocalStorage(musicFileSyncResultListener)

    suspend fun saveThresholds(
        lowThresholdInput: String?,
        highThresholdInput: String?,
        thresholdSaveResultListener: IThresholdSaveResultListener
    ) {
        if (!isValidThreshold(lowThresholdInput) || !isValidThreshold(highThresholdInput)) {
            thresholdSaveResultListener.onThresholdSaveResult(R.string.InvalidNumber)
            return
        }

        val newLowThresholdValue = lowThresholdInput?.toFloatOrNull()
        val newHighThresholdValue = highThresholdInput?.toFloatOrNull()
        if (newLowThresholdValue != null && newHighThresholdValue != null) {
            if (newLowThresholdValue >= newHighThresholdValue) {
                thresholdSaveResultListener.onThresholdSaveResult(R.string.HighThresholdValueShouldBeHigher)
                return
            } else {
                saveLowSpeedThreshold(newLowThresholdValue)
                saveHighSpeedThreshold(newHighThresholdValue)
            }
        }
        newLowThresholdValue?.let {
            if (it >= highSpeedThreshold) {
                thresholdSaveResultListener.onThresholdSaveResult(R.string.HighThresholdValueShouldBeHigher)
                return
            } else {
                saveLowSpeedThreshold(newLowThresholdValue)
            }
        }
        newHighThresholdValue?.let {
            if (it <= lowSpeedThreshold) {
                thresholdSaveResultListener.onThresholdSaveResult(R.string.HighThresholdValueShouldBeHigher)
                return
            } else {
                saveHighSpeedThreshold(newHighThresholdValue)
            }
        }
        thresholdSaveResultListener.onThresholdSaveResult(R.string.ThresholdRecordSuccess)
    }

    private suspend fun saveLowSpeedThreshold(newLowThresholdValue: Float) {
        dataStoreManager.saveFloat(DataStoreKeys.KEY_LOW_SPEED_THRESHOLD, lowSpeedThreshold)
        lowSpeedThreshold = newLowThresholdValue
    }

    private suspend fun saveHighSpeedThreshold(newHighThresholdValue: Float) {
        dataStoreManager.saveFloat(DataStoreKeys.KEY_HIGH_SPEED_THRESHOLD, highSpeedThreshold)
        highSpeedThreshold = newHighThresholdValue
    }

    private fun isValidThreshold(thresholdInput: String?): Boolean {
        if (thresholdInput.isNullOrBlank()) {
            return true
        }

        val thresholdValue = thresholdInput.toFloatOrNull()
        if (thresholdValue == null || thresholdValue <= 0 || thresholdValue > THRESHOLD_MAX_VALUE) {
            return false
        }
        return true
    }
}

interface IThresholdSaveResultListener {
    fun onThresholdSaveResult(resultInfoResId: Int)
}
