package com.dobra.fitmusic.dialog

import android.app.Dialog
import android.os.Bundle
import android.util.SparseBooleanArray
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.app.AppCompatDialogFragment
import com.dobra.fitmusic.R
import com.dobra.fitmusic.extension.name
import com.dobra.fitmusic.model.Song
import com.dobra.fitmusic.model.relation.PlaylistWithSongs
import com.dobra.fitmusic.model.relation.mediaSourceAt
import com.dobra.fitmusic.model.relation.size
import com.dobra.fitmusic.util.ViewUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val PLAYLIST_SONG_SELECTION_DIALOG_TAG = "PLAYLIST_SONG_SELECTION_DIALOG_TAG"

@AndroidEntryPoint
class SongSelectionDialog(
    private val allSongsPlaylist: PlaylistWithSongs,
    private val checkedSongs: List<Song>?,
    private val songSelectionListener: ISongSelectionListener
) : AppCompatDialogFragment() {
    @Inject
    lateinit var viewUtil: ViewUtil

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = MaterialAlertDialogBuilder(requireContext())

        val inflater = requireActivity().layoutInflater
        val view = inflater.inflate(R.layout.dialog_song_selection, null)

        val listViewSongSelection = view.findViewById<ListView>(R.id.lvSongSelection)
        listViewSongSelection.adapter =
            ArrayAdapter(
                requireActivity(),
                android.R.layout.simple_list_item_multiple_choice,
                getNamesOfAllSongs()
            )

        val titleView = viewUtil.createDialogTitleView(inflater, getString(R.string.SelectSongs))

        setCheckedSongs(listViewSongSelection)

        builder.setView(view)
            .setCustomTitle(titleView)
            .setNegativeButton(getString(R.string.Cancel)) { _, _ ->
                songSelectionListener.onSongSelectionDialogDismissed()
            }
            .setPositiveButton(R.string.Confirm) { _, _ ->
                songSelectionListener.onSongsSelected(listViewSongSelection.checkedItemPositions)
            }

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }

    private fun setCheckedSongs(listViewSongSelection: ListView) {
        checkedSongs?.forEach { checkedSong ->
            val checkedSongIndex = allSongsPlaylist.songs.indexOfFirst {
                it.songUriId == checkedSong.songUriId
            }
            listViewSongSelection.setItemChecked(checkedSongIndex, true)
        }
    }

    private fun getNamesOfAllSongs(): List<String> {
        val songNames = arrayListOf<String>()

        for (i in 0 until allSongsPlaylist.size()) {
            songNames.add(allSongsPlaylist.mediaSourceAt(i).name())
        }
        return songNames
    }
}

interface ISongSelectionListener {
    fun onSongsSelected(selectedSongIndexes: SparseBooleanArray)
    fun onSongSelectionDialogDismissed()
}
