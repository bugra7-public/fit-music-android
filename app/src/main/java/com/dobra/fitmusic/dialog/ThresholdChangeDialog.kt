package com.dobra.fitmusic.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialogFragment
import com.dobra.fitmusic.R
import com.dobra.fitmusic.common.lowSpeedThreshold
import com.dobra.fitmusic.common.highSpeedThreshold
import com.dobra.fitmusic.util.ViewUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val THRESHOLD_CHANGE_DIALOG_TAG = "THRESHOLD_CHANGE_DIALOG_TAG"

@AndroidEntryPoint
class ThresholdChangeDialog(
    private val thresholdInputListener: IThresholdInputListener,
    private val titleText: String
) : AppCompatDialogFragment() {
    @Inject
    lateinit var viewUtil: ViewUtil

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = MaterialAlertDialogBuilder(requireContext())
        val inflater = requireActivity().layoutInflater
        val titleView = viewUtil.createDialogTitleView(inflater, titleText)

        val dialogView = inflater.inflate(R.layout.dialog_threshold_change, null)
        val lowThresholdEditText =
            dialogView.findViewById<TextInputEditText>(R.id.etLowThresholdValue)
        val highThresholdEditText =
            dialogView.findViewById<TextInputEditText>(R.id.etHighThresholdValue)
        lowThresholdEditText?.hint = lowSpeedThreshold.toString()
        highThresholdEditText?.hint = highSpeedThreshold.toString()

        builder.setView(dialogView)
            .setCustomTitle(titleView)
            .setNegativeButton(R.string.Cancel) { _, _ ->
            }
            .setPositiveButton(R.string.Confirm) { _, _ ->
                thresholdInputListener.onThresholdInputsEntered(
                    lowThresholdEditText.text?.toString(),
                    highThresholdEditText.text?.toString()
                )
            }
        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }
}

interface IThresholdInputListener {
    fun onThresholdInputsEntered(lowThresholdInput: String?, highThresholdInput: String?)
}
