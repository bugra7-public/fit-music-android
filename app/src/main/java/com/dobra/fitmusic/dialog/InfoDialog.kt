package com.dobra.fitmusic.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialogFragment
import com.dobra.fitmusic.R
import com.dobra.fitmusic.util.ViewUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textview.MaterialTextView
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class InfoDialog(
    private val titleText: String,
    private val bodyText: String
) : AppCompatDialogFragment() {
    @Inject
    lateinit var viewUtil: ViewUtil

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = MaterialAlertDialogBuilder(requireContext())

        val inflater = requireActivity().layoutInflater

        val titleView = viewUtil.createDialogTitleView(inflater, titleText)

        val dialogView = inflater.inflate(R.layout.dialog_info, null)

        val tvInfo = dialogView.findViewById<MaterialTextView>(R.id.tvInfo)
        tvInfo.text = bodyText

        builder.setView(dialogView)
            .setCustomTitle(titleView)
            .setPositiveButton(R.string.Okay, null)

        return builder.create()
    }
}
