package com.dobra.fitmusic.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialogFragment
import com.dobra.fitmusic.R
import com.dobra.fitmusic.util.ViewUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val PLAYLIST_NAME_DIALOG_TAG = "PLAYLIST_NAME_DIALOG_TAG"

@AndroidEntryPoint
class PlaylistNameDialog(
    private val playlistNameInputListener: IPlaylistNameInputListener,
    private val titleText: String
) : AppCompatDialogFragment() {
    @Inject
    lateinit var viewUtil: ViewUtil

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = MaterialAlertDialogBuilder(requireContext())

        val inflater = requireActivity().layoutInflater

        val titleView = viewUtil.createDialogTitleView(inflater, titleText)

        builder.setView(inflater.inflate(R.layout.dialog_playlist_name, null))
            .setCustomTitle(titleView)
            .setNegativeButton(R.string.Cancel) { _, _ ->
                playlistNameInputListener.onPlaylistNameDialogDismissed()
            }
            .setPositiveButton(R.string.Confirm) { _, _ ->
                playlistNameInputListener.onPlaylistNameInputEntered(
                    dialog!!.findViewById<TextInputEditText>(R.id.etPlaylistName).text.toString()
                )
            }

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }
}

interface IPlaylistNameInputListener {
    fun onPlaylistNameInputEntered(playlistNameInput: String?)
    fun onPlaylistNameDialogDismissed()
}
