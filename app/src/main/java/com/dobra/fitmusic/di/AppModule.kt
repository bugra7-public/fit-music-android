package com.dobra.fitmusic.di

import android.Manifest
import android.content.Context
import com.dobra.fitmusic.dao.IPlaylistDao
import com.dobra.fitmusic.dao.IPlaylistSongCrossRefDao
import com.dobra.fitmusic.dao.IRunningSessionDao
import com.dobra.fitmusic.dao.ISongDao
import com.dobra.fitmusic.manager.AudioDataManager
import com.dobra.fitmusic.manager.AutomaticSongChangeManager
import com.dobra.fitmusic.manager.DataStoreManager
import com.dobra.fitmusic.manager.MusicPlayerManager
import com.dobra.fitmusic.manager.RoomDatabaseManager
import com.dobra.fitmusic.manager.RunningSessionManager
import com.dobra.fitmusic.manager.SongSyncManager
import com.dobra.fitmusic.util.CalculationUtil
import com.dobra.fitmusic.util.MusicFileUtil
import com.dobra.fitmusic.util.ViewUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideViewUtil(): ViewUtil {
        return ViewUtil()
    }

    @Singleton
    @Provides
    fun provideMusicPlayerManager(
        @ApplicationContext context: Context
    ): MusicPlayerManager {
        return MusicPlayerManager(context)
    }

    @Singleton
    @Provides
    fun provideRunningSessionManager(
        @ApplicationContext context: Context,
        calculationUtil: CalculationUtil
    ): RunningSessionManager {
        return RunningSessionManager(context, calculationUtil)
    }

    @Singleton
    @Provides
    fun provideCalculationUtil(): CalculationUtil {
        return CalculationUtil()
    }

    @Singleton
    @Provides
    fun providePlaylistDao(
        @ApplicationContext context: Context
    ): IPlaylistDao {
        return RoomDatabaseManager.getInstance(context).playlistDao
    }

    @Singleton
    @Provides
    fun provideSongDao(
        @ApplicationContext context: Context
    ): ISongDao {
        return RoomDatabaseManager.getInstance(context).songDao
    }

    @Singleton
    @Provides
    fun providePlaylistSongCrossRefDao(
        @ApplicationContext context: Context
    ): IPlaylistSongCrossRefDao {
        return RoomDatabaseManager.getInstance(context).playlistSongCrossRefDao
    }

    @Singleton
    @Provides
    fun provideRunningSessionDaoDao(
        @ApplicationContext context: Context
    ): IRunningSessionDao {
        return RoomDatabaseManager.getInstance(context).runningSessionDao
    }

    @Singleton
    @Provides
    fun provideLocationPermissions(): Array<String> {
        val permissions: MutableList<String> = mutableListOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        return permissions.toTypedArray()
    }

    @Singleton
    @Provides
    fun provideAudioData(
        @ApplicationContext context: Context,
        calculationUtil: CalculationUtil
    ): AudioDataManager {
        return AudioDataManager(context, calculationUtil)
    }

    @Singleton
    @Provides
    fun provideDataStoreManager(
        @ApplicationContext context: Context
    ): DataStoreManager {
        return DataStoreManager(context)
    }

    @Singleton
    @Provides
    fun provideAutomaticSongChangeManager(
        musicPlayerManager: MusicPlayerManager,
        audioDataManager: AudioDataManager
    ): AutomaticSongChangeManager {
        return AutomaticSongChangeManager(musicPlayerManager, audioDataManager)
    }

    @Singleton
    @Provides
    fun provideSongSyncManager(
        musicFileUtil: MusicFileUtil, audioDataManager: AudioDataManager, fitMusicDao: IPlaylistDao
    ): SongSyncManager {
        return SongSyncManager(musicFileUtil, audioDataManager, fitMusicDao)
    }

    @Singleton
    @Provides
    fun provideMusicFileUtil(@ApplicationContext context: Context): MusicFileUtil {
        return MusicFileUtil(context)
    }
}
