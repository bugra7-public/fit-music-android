package com.dobra.fitmusic.activity

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.dobra.fitmusic.R
import com.dobra.fitmusic.common.IGNORED_INDEX
import com.dobra.fitmusic.dialog.IConfirmationDialogListener
import com.dobra.fitmusic.util.ViewUtil
import com.dobra.fitmusic.vm.PermissionActivityViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

private const val RUNTIME_PERMISSIONS_REQUEST_CODE = 1000

@AndroidEntryPoint
class PermissionActivity : AppCompatActivity(), IConfirmationDialogListener {
    @Inject
    lateinit var viewUtil: ViewUtil

    private val viewModel: PermissionActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestFilePermissions()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        val permissionsGranted = viewUtil.checkPermissionsGranted(
            viewModel.permissionsToBeGrantedAtRuntime,
            permissions,
            grantResults
        )

        if (permissionsGranted) {
            viewUtil.changeActivity(this@PermissionActivity, MainActivity::class.java)
        } else {
            viewUtil.createConfirmationDialog(
                getString(R.string.PermissionRequired),
                getString(R.string.AllowPermissions),
                IGNORED_INDEX,
                this,
                supportFragmentManager
            )
        }
    }

    override fun onDialogConfirmed(index: Int) {
        startActivity(Intent.makeRestartActivityTask(this.intent.component))
    }

    override fun onDialogRejected() {
        finish()
    }

    private fun requestFilePermissions() {
        ActivityCompat.requestPermissions(
            this,
            viewModel.permissionsToBeGrantedAtRuntime,
            RUNTIME_PERMISSIONS_REQUEST_CODE
        )
    }
}
