package com.dobra.fitmusic.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dobra.fitmusic.util.ViewUtil
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {
    // TODO: https://developer.android.com/guide/topics/ui/splash-screen
    @Inject
    lateinit var viewUtil: ViewUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewUtil.changeActivity(this@SplashActivity, PermissionActivity::class.java)
    }
}
