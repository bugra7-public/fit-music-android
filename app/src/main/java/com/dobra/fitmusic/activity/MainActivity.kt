package com.dobra.fitmusic.activity

import android.Manifest
import android.annotation.SuppressLint
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.dobra.fitmusic.R
import com.dobra.fitmusic.databinding.ActivityMainBinding
import com.dobra.fitmusic.dialog.IConfirmationDialogListener
import com.dobra.fitmusic.extension.isPermissionGranted
import com.dobra.fitmusic.manager.CustomFragmentManager
import com.dobra.fitmusic.model.RunningModeState
import com.dobra.fitmusic.common.IGNORED_INDEX
import com.dobra.fitmusic.util.ViewUtil
import com.dobra.fitmusic.vm.MainActivityViewModel
import com.google.android.material.navigation.NavigationBarView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val NULL_TEXT = "null"
const val MAIN_LOCATION_PERMISSIONS_REQ_CODE = 5000
const val BACKGROUND_LOCATION_PERMISSION_REQ_CODE = 5001

@Suppress("TooManyFunctions")
@AndroidEntryPoint
class MainActivity :
    AppCompatActivity(),
    NavigationBarView.OnItemSelectedListener,
    IConfirmationDialogListener {
    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var bind: ActivityMainBinding

    @Inject
    lateinit var viewUtil: ViewUtil

    @Inject
    lateinit var customFragmentManager: CustomFragmentManager

    @Inject
    lateinit var locationPermissions: Array<String>

    @SuppressLint("InlinedApi")
    private val backgroundLocationPermission = Manifest.permission.ACCESS_BACKGROUND_LOCATION

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bind.root)

        if (savedInstanceState == null) {
            customFragmentManager.loadSongsFragment(supportFragmentManager)
            bind.bottomNavigationView.selectedItemId = R.id.nav_songs
        }
        bind.bottomNavigationView.setOnItemSelectedListener(this@MainActivity)
        bind.tvSongName.isSelected = true // for sliding text

        if (viewModel.audioDataManager.allPlaylistsWithSongs.isEmpty()) {
            val allSongs = viewModel.getAllSongsFromStorage()
            viewModel.createPlaylists(
                allSongs = allSongs,
                allSongsName = getString(R.string.AllSongs),
                slowSongsName = getString(R.string.SlowSongs),
                fastSongsName = getString(R.string.FastSongs)
            )
            viewModel.savePlaylistsToLocalDatabase()
            viewModel.setInitialPlaylist()
        } else {
            viewModel.setConcatenatingMediaSourcesOfPlaylists()
        }

        launchPlayerObservers()
        launchRunningObservers()

        setUpSlider()

        setOnClickListeners()

        onBackPressedDispatcher.addCallback(
            this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (customFragmentManager.isOnBottomNavigationFragment()) {
                        finish()
                    } else {
                        customFragmentManager.goBackToPreviousFragment(supportFragmentManager)
                    }
                }
            }
        )
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.nav_playlists -> {
                customFragmentManager.loadPlaylistsFragment(supportFragmentManager)
            }
            R.id.nav_songs -> {
                customFragmentManager.loadSongsFragment(supportFragmentManager)
            }
            R.id.nav_profile -> {
                customFragmentManager.loadProfileFragment(supportFragmentManager)
            }
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        volumeControlStream = AudioManager.STREAM_MUSIC
    }

    override fun onPause() {
        viewModel.saveApplicationState()
        super.onPause()
    }

    override fun onDestroy() {
        if (viewModel.runningSessionManager.isRunningModeActive()) {
            viewModel.endRunningSession()
            getRunningModeState().value = RunningModeState.FINISHED
        }
        viewModel.stopPlayWhenReadyFeatureOfMusicPlayer()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        val mainLocationPermissionGranted = viewUtil.checkPermissionsGranted(
            locationPermissions,
            permissions,
            grantResults
        )

        if (!mainLocationPermissionGranted) {
            viewUtil.createInfoDialog(
                getString(R.string.LocationPermissionNeeded),
                getString(R.string.LocationRequired),
                supportFragmentManager
            )
            getRunningModeState().value = RunningModeState.REJECTED_BY_LOCATION_PERMISSION
            return
        }

        val backgroundLocationPermissionGranted =
            this.isPermissionGranted(backgroundLocationPermission)

        when (requestCode) {
            MAIN_LOCATION_PERMISSIONS_REQ_CODE -> {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q ||
                    backgroundLocationPermissionGranted
                ) {
                    getRunningModeState().value = RunningModeState.RUNNING
                } else {
                    viewUtil.createConfirmationDialog(
                        getString(R.string.BackgroundLocationTitle),
                        getString(R.string.BackgroundLocationRequest),
                        IGNORED_INDEX,
                        this,
                        supportFragmentManager
                    )
                }
            }
            BACKGROUND_LOCATION_PERMISSION_REQ_CODE -> {
                if (backgroundLocationPermissionGranted) {
                    getRunningModeState().value = RunningModeState.RUNNING
                } else {
                    viewUtil.createInfoDialog(
                        getString(R.string.LocationPermissionNeeded),
                        getString(R.string.LocationRequired),
                        supportFragmentManager
                    )
                    getRunningModeState().value = RunningModeState.REJECTED_BY_LOCATION_PERMISSION
                }
            }
        }
    }

    override fun onDialogConfirmed(index: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(backgroundLocationPermission),
                BACKGROUND_LOCATION_PERMISSION_REQ_CODE
            )
        }
    }

    override fun onDialogRejected() {
        getRunningModeState().value = RunningModeState.REJECTED_BY_LOCATION_PERMISSION
    }

    private fun getColorById(colorId: Int): Int {
        return ContextCompat.getColor(this, colorId)
    }

    private fun getRunningModeState(): MutableStateFlow<RunningModeState> =
        viewModel.runningSessionManager.runningModeState

    private fun setUpSlider() {
        bind.slider.valueFrom = 0F
        bind.slider.setLabelFormatter { durationInMilliseconds ->
            viewModel.formatSongDuration(durationInMilliseconds)
        }
        bind.slider.addOnChangeListener { _, value, fromUser ->
            if (fromUser) {
                viewModel.seekSongTo(value.toLong())
                bind.currentDuration.text = viewModel.formatSongDuration(value)
            }
        }
    }

    private fun setOnClickListeners() {
        bind.btnPlayPause.setOnClickListener {
            viewModel.processPlayButtonClick()
        }
        bind.btnNext.setOnClickListener {
            viewModel.processNextButtonClick()
        }
        bind.btnPrevious.setOnClickListener {
            viewModel.processPreviousButtonClick()
        }
        bind.btnShuffle.setOnClickListener {
            viewModel.processShuffleButtonClick()
        }
        bind.btnRepeat.setOnClickListener {
            viewModel.processRepeatButtonClick()
        }
    }

    private fun launchPlayerObservers() {
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
                launch {
                    viewModel.isMusicPlaying.collectLatest { isPlaying ->
                        if (isPlaying) {
                            bind.btnPlayPause.setImageResource(R.drawable.ic_pause)
                        } else {
                            bind.btnPlayPause.setImageResource(R.drawable.ic_play)
                        }
                    }
                }
                launch {
                    viewModel.repeatEnabled.collectLatest {
                        if (it) {
                            bind.btnRepeat.setColorFilter(getColorById(R.color.fit_turquoise))
                        } else {
                            bind.btnRepeat.setColorFilter(getColorById(R.color.fit_white))
                        }
                    }
                }
                launch {
                    viewModel.shuffleEnabled.collectLatest {
                        if (it) {
                            bind.btnShuffle.setColorFilter(getColorById(R.color.fit_turquoise))
                        } else {
                            bind.btnShuffle.setColorFilter(getColorById(R.color.fit_white))
                        }
                    }
                }
                launch {
                    viewModel.currentSongDuration.collectLatest {
                        bind.slider.value = 0F
                        bind.slider.valueTo = it.toFloat()
                        bind.currentDuration.text = getString(R.string.DefaultDuration)
                        bind.totalDuration.text = viewModel.formatSongDuration(it.toFloat())
                    }
                }
                launch {
                    viewModel.audioCurrentPositionFlow.collectLatest {
                        if (it > 0 && it <= bind.slider.valueTo) {
                            bind.slider.value = it
                            bind.currentDuration.text = viewModel.formatSongDuration(it)
                        }
                    }
                }
            }
        }

        viewModel.currentSongName.observe(this) { songName ->
            if (songName != null && !songName.equals(NULL_TEXT)) {
                bind.tvSongName.text = songName
            } else {
                bind.tvSongName.text = getString(R.string.SongNotFound)
            }
        }
    }

    private fun launchRunningObservers() {
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
                launch {
                    getRunningModeState().collectLatest {
                        if (it == RunningModeState.RUNNING) {
                            if (!viewModel.shuffleEnabled.value) {
                                viewModel.processShuffleButtonClick()
                            }
                            viewModel.startRunningSession()
                        } else if (it == RunningModeState.FINISHED) {
                            viewModel.endRunningSession()
                        }
                        bind.currentSpeedLayout.visibility =
                            if (viewModel.runningSessionManager.isRunningModeActive()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }
                        bind.lineCurrentSpeed.visibility =
                            if (viewModel.runningSessionManager.isRunningModeActive()) {
                                View.VISIBLE
                            } else {
                                View.GONE
                            }
                    }
                }
                launch {
                    viewModel.runningSessionManager.currentSpeed.collectLatest {
                        if (getRunningModeState().value == RunningModeState.RUNNING) {
                            bind.tvCurrentSpeed.text = it.toString()
                        }
                    }
                }
            }
        }
    }
}
