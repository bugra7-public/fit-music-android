package com.dobra.fitmusic.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.dobra.fitmusic.R
import com.dobra.fitmusic.databinding.FragmentMapBinding
import com.dobra.fitmusic.vm.RunningHistoryAndMapSharedViewModel
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Polyline
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapFragment : Fragment() {
    private val viewModel: RunningHistoryAndMapSharedViewModel by activityViewModels()
    private lateinit var bind: FragmentMapBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpMap()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = FragmentMapBinding.inflate(inflater, container, false)
        bind.mapView.onStart()

        createPolyline()
        return bind.root
    }

    override fun onDestroy() {
        killMap()
        super.onDestroy()
    }

    private fun setUpMap() {
        try {
            MapKitFactory.setApiKey(getString(R.string.YandexMapKitApiKey))
        } catch (ignored: Throwable) {
            /* To bypass "setApiKey() should be called before initialize()" error which happens
            when creating a second MapFragment */
        }
        MapKitFactory.initialize(requireContext())
        MapKitFactory.getInstance().onStart()
    }

    private fun createPolyline() {
        val polyline = Polyline(viewModel.createPolylinePoints())
        bind.mapView.map.mapObjects.addPolyline(polyline)

        if (polyline.points.isNotEmpty()) {
            val startPoint = polyline.points.first()
            val endPoint = polyline.points.last()
            bind.mapView.map.mapObjects.addPlacemark(startPoint)
            bind.mapView.map.mapObjects.addPlacemark(endPoint)
            bind.mapView.map.move(
                bind.mapView.map.cameraPosition(viewModel.createBoundingBox(polyline.points))
            )
        }
    }

    private fun killMap() {
        if (::bind.isInitialized) {
            bind.mapView.onStop()
        }
        MapKitFactory.getInstance().onStop()
    }
}
