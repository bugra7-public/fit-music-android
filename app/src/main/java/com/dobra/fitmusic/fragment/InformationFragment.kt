package com.dobra.fitmusic.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dobra.fitmusic.databinding.FragmentInformationBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InformationFragment : Fragment() {
    private lateinit var bind: FragmentInformationBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = FragmentInformationBinding.inflate(inflater, container, false)
        bind.tvYandexMapsTermsOfUse.setOnClickListener {
            openYandexTermsOfUseLink()
        }
        return bind.root
    }

    private fun openYandexTermsOfUseLink() {
        try {
            val uri = Uri.parse(bind.tvYandexMapsTermsOfUse.text.toString())
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        } catch (ignored: Exception) {
        }
    }
}
