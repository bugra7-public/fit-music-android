package com.dobra.fitmusic.fragment

import android.os.Bundle
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.dobra.fitmusic.R
import com.dobra.fitmusic.adapter.IPlaylistAdapterListener
import com.dobra.fitmusic.adapter.PlaylistAdapter
import com.dobra.fitmusic.databinding.FragmentPlaylistsBinding
import com.dobra.fitmusic.dialog.IConfirmationDialogListener
import com.dobra.fitmusic.dialog.IPlaylistNameInputListener
import com.dobra.fitmusic.dialog.ISongSelectionListener
import com.dobra.fitmusic.dialog.PLAYLIST_NAME_DIALOG_TAG
import com.dobra.fitmusic.dialog.PLAYLIST_SONG_SELECTION_DIALOG_TAG
import com.dobra.fitmusic.dialog.PlaylistNameDialog
import com.dobra.fitmusic.dialog.SongSelectionDialog
import com.dobra.fitmusic.model.ValidationModel
import com.dobra.fitmusic.common.IGNORED_INDEX
import com.dobra.fitmusic.util.ViewUtil
import com.dobra.fitmusic.vm.PlaylistFragmentViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PlaylistFragment :
    Fragment(),
    IPlaylistNameInputListener,
    ISongSelectionListener,
    IPlaylistAdapterListener,
    IConfirmationDialogListener {
    private val viewModel: PlaylistFragmentViewModel by viewModels()

    private lateinit var bind: FragmentPlaylistsBinding

    @Inject
    lateinit var viewUtil: ViewUtil

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = FragmentPlaylistsBinding.inflate(inflater, container, false)

        buildRecyclerView(activity)

        bind.addPlaylistButton.setOnClickListener {
            createPlaylistNameDialog(getString(R.string.CreatePlaylist))
        }
        return bind.root
    }

    private fun buildRecyclerView(activity: FragmentActivity?) {
        val playlistAdapter = PlaylistAdapter(viewModel.allPlaylists(), this)
        bind.rclPlaylists.layoutManager = LinearLayoutManager(activity)
        bind.rclPlaylists.adapter = playlistAdapter
    }

    private fun createPlaylistNameDialog(titleText: String) {
        val playlistNameDialog = PlaylistNameDialog(this, titleText)
        playlistNameDialog.show(childFragmentManager, PLAYLIST_NAME_DIALOG_TAG)
    }

    private fun createPlaylistSongSelectionDialog() {
        val songSelectionDialog = SongSelectionDialog(
            viewModel.getAllSongs(),
            viewModel.getEditingPlaylistSongs(),
            this
        )
        songSelectionDialog.show(childFragmentManager, PLAYLIST_SONG_SELECTION_DIALOG_TAG)
    }

    override fun onPlaylistNameInputEntered(playlistNameInput: String?) {
        val validationModel = viewModel.isPlaylistNameValid(playlistNameInput)

        if (!validationModel.valid) {
            viewUtil.createInfoDialog(
                getString(R.string.InvalidName),
                getString(validationModel.errorMessageId),
                childFragmentManager
            )
        } else {
            if (viewModel.isPlaylistEditOngoing()) {
                viewModel.renamePlaylist(playlistNameInput!!)
                bind.rclPlaylists.adapter?.notifyItemChanged(viewModel.editingPlaylistIndex())
            } else {
                viewModel.newPlaylistName = playlistNameInput!!
                createPlaylistSongSelectionDialog()
            }
        }
        viewModel.setPlaylistEditOngoing(false, IGNORED_INDEX)
    }

    override fun onSongsSelected(selectedSongIndexes: SparseBooleanArray) {
        if (!viewModel.isPlaylistEditOngoing()) {
            viewModel.createNewPlaylist(viewModel.newPlaylistName, selectedSongIndexes)
            bind.rclPlaylists.adapter?.notifyItemInserted(viewModel.allPlaylists().size - 1)
        } else {
            viewModel.updatePlaylistSongs(selectedSongIndexes)
            bind.rclPlaylists.adapter?.notifyItemChanged(viewModel.editingPlaylistIndex())
            viewModel.setPlaylistEditOngoing(false, IGNORED_INDEX)
        }
    }

    override fun onDialogConfirmed(index: Int) {
        viewModel.deletePlaylist(index)
        bind.rclPlaylists.adapter?.notifyDataSetChanged()
    }

    override fun onPlaylistNameDialogDismissed() {
        viewModel.setPlaylistEditOngoing(false, IGNORED_INDEX)
    }

    override fun onSongSelectionDialogDismissed() {
        viewModel.setPlaylistEditOngoing(false, IGNORED_INDEX)
    }

    override fun showPlaylistMenu(index: Int, playlistName: String, view: View) {
        val popupMenu = PopupMenu(requireContext(), view)

        popupMenu.inflate(R.menu.playlist_item_popup_menu)
        popupMenu.setForceShowIcon(true)

        popupMenu.setOnMenuItemClickListener {
            it.title?.let { title ->
                viewUtil.changeTextColor(
                    title,
                    ContextCompat.getColor(requireContext(), R.color.fit_turquoise)
                )
            }

            var validationModel = ValidationModel(true, 0)
            if (it.itemId == R.id.popup_activate) {
                viewModel.changeActivePlaylist(index)
            } else if (it.itemId == R.id.popup_rename) {
                validationModel = viewModel.isRenameAllowed(index)
                if (validationModel.valid) {
                    viewModel.setPlaylistEditOngoing(true, index)
                    createPlaylistNameDialog(getString(R.string.RenamePlaylist))
                }
            } else if (it.itemId == R.id.popup_change_songs) {
                validationModel = viewModel.isChangeSongsAllowed(index)
                if (validationModel.valid) {
                    viewModel.setPlaylistEditOngoing(true, index)
                    createPlaylistSongSelectionDialog()
                }
            } else if (it.itemId == R.id.popup_delete) {
                validationModel = viewModel.isDeleteAllowed(index)
                if (validationModel.valid) {
                    viewUtil.createConfirmationDialog(
                        getString(R.string.ConfirmDeletion),
                        getString(R.string.DoYouWantToDeletePlaylistThisCannotBeUndone),
                        index,
                        this,
                        childFragmentManager
                    )
                }
            }

            if (!validationModel.valid) {
                viewUtil.createInfoDialog(
                    getString(R.string.InvalidOperation),
                    getString(validationModel.errorMessageId),
                    childFragmentManager
                )
            }
            return@setOnMenuItemClickListener true
        }
        popupMenu.show()
    }
}
