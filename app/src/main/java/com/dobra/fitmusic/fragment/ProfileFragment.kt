package com.dobra.fitmusic.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.dobra.fitmusic.R
import com.dobra.fitmusic.common.IGNORED_INDEX
import com.dobra.fitmusic.common.INFO_DIALOG_TAG
import com.dobra.fitmusic.databinding.FragmentProfileBinding
import com.dobra.fitmusic.dialog.IConfirmationDialogListener
import com.dobra.fitmusic.dialog.IThresholdInputListener
import com.dobra.fitmusic.dialog.InfoDialog
import com.dobra.fitmusic.dialog.THRESHOLD_CHANGE_DIALOG_TAG
import com.dobra.fitmusic.dialog.ThresholdChangeDialog
import com.dobra.fitmusic.manager.CustomFragmentManager
import com.dobra.fitmusic.manager.IMusicFileSyncResultListener
import com.dobra.fitmusic.util.ViewUtil
import com.dobra.fitmusic.vm.IThresholdSaveResultListener
import com.dobra.fitmusic.vm.ProfileFragmentViewModel
import com.google.android.play.core.review.ReviewManagerFactory
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment :
    Fragment(),
    IThresholdInputListener,
    IThresholdSaveResultListener,
    IConfirmationDialogListener,
    IMusicFileSyncResultListener {

    private lateinit var bind: FragmentProfileBinding
    private val viewModel: ProfileFragmentViewModel by viewModels()

    @Inject
    lateinit var customFragmentManager: CustomFragmentManager

    @Inject
    lateinit var viewUtil: ViewUtil

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = FragmentProfileBinding.inflate(inflater, container, false)

        bind.layoutRunningHistory.setOnClickListener {
            customFragmentManager.loadFragment(RunningHistoryFragment(), parentFragmentManager)
        }
        bind.layoutSpeedThresholds.setOnClickListener {
            ThresholdChangeDialog(this, getString(R.string.ChangeThreshold))
                .show(childFragmentManager, THRESHOLD_CHANGE_DIALOG_TAG)
        }
        bind.layoutSyncMusicFiles.setOnClickListener {
            viewUtil.createConfirmationDialog(
                getString(R.string.SyncMusicFiles),
                getString(R.string.SyncMusicFilesConfirmation),
                IGNORED_INDEX,
                this,
                childFragmentManager
            )
        }
        bind.layoutAppInformation.setOnClickListener {
            customFragmentManager.loadFragment(InformationFragment(), parentFragmentManager)
        }
        bind.layoutRateApplication.setOnClickListener {
            createInAppReviewFlow()
        }
        return bind.root
    }

    private fun createInAppReviewFlow() {
        val manager = ReviewManagerFactory.create(requireContext())
        val request = manager.requestReviewFlow()
        request.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val reviewInfo = task.result
                manager.launchReviewFlow(requireActivity(), reviewInfo)
            }
        }
    }

    override fun onThresholdInputsEntered(lowThresholdInput: String?, highThresholdInput: String?) {
        lifecycleScope.launch {
            viewModel.saveThresholds(lowThresholdInput, highThresholdInput, this@ProfileFragment)
        }
    }

    override fun onThresholdSaveResult(resultInfoResId: Int) {
        val infoDialog = InfoDialog(getString(R.string.ChangeThreshold), getString(resultInfoResId))
        infoDialog.show(childFragmentManager, INFO_DIALOG_TAG)
    }

    override fun onDialogConfirmed(index: Int) {
        viewModel.syncSongsWithLocalStorage(this)
    }

    override fun onMusicFilesSynced() {
        viewUtil.createInfoDialog(
            getString(R.string.SyncMusicFiles),
            getString(R.string.SyncMusicSuccess),
            childFragmentManager
        )
    }
}
