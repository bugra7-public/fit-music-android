package com.dobra.fitmusic.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.dobra.fitmusic.adapter.IRunningHistoryAdapterListener
import com.dobra.fitmusic.adapter.RunningHistoryAdapter
import com.dobra.fitmusic.databinding.FragmentRunningHistoryBinding
import com.dobra.fitmusic.manager.CustomFragmentManager
import com.dobra.fitmusic.model.RunningMoment
import com.dobra.fitmusic.model.relation.RunningSessionWithMoments
import com.dobra.fitmusic.vm.RunningHistoryAndMapSharedViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RunningHistoryFragment : Fragment(), IRunningHistoryAdapterListener {
    //TODO: add delete functionality
    private val viewModel: RunningHistoryAndMapSharedViewModel by activityViewModels()

    private lateinit var bind: FragmentRunningHistoryBinding

    @Inject
    lateinit var customFragmentManager: CustomFragmentManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = FragmentRunningHistoryBinding.inflate(inflater, container, false)

        lifecycleScope.launch {
            viewModel.runningSessionWithMomentsList.collectLatest { runningSessionList ->
                if (runningSessionList.isNotEmpty()) {
                    buildRecyclerView(runningSessionList, activity)
                } else {
                    bind.tvRunningHistoryNotFound.visibility = View.VISIBLE
                }
                bind.progressBar.visibility = View.GONE
            }
        }
        return bind.root
    }

    private fun buildRecyclerView(
        runningSessionList: List<RunningSessionWithMoments>,
        activity: FragmentActivity?
    ) {
        val playlistAdapter = RunningHistoryAdapter(runningSessionList, this)
        bind.rclRunningList.layoutManager = LinearLayoutManager(activity)
        bind.rclRunningList.adapter = playlistAdapter
    }

    override fun onRunningHistoryItemSelected(runningMoments: List<RunningMoment>) {
        viewModel.selectedRunningSessionMoments = runningMoments
        openMapFragment()
    }

    private fun openMapFragment() {
        customFragmentManager.loadFragment(MapFragment(), parentFragmentManager)
    }
}
