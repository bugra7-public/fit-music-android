package com.dobra.fitmusic.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import androidx.core.app.ActivityCompat
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.dobra.fitmusic.R
import com.dobra.fitmusic.activity.MAIN_LOCATION_PERMISSIONS_REQ_CODE
import com.dobra.fitmusic.adapter.SongAdapter
import com.dobra.fitmusic.databinding.FragmentSongsBinding
import com.dobra.fitmusic.dialog.IConfirmationDialogListener
import com.dobra.fitmusic.manager.IMusicFileSyncResultListener
import com.dobra.fitmusic.manager.RunningSessionManager
import com.dobra.fitmusic.model.RunningModeState
import com.dobra.fitmusic.model.relation.concatenatingMediaSource
import com.dobra.fitmusic.model.relation.isEmpty
import com.dobra.fitmusic.model.relation.name
import com.dobra.fitmusic.common.IGNORED_INDEX
import com.dobra.fitmusic.util.ViewUtil
import com.dobra.fitmusic.vm.SongsFragmentViewModel
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SongsFragment :
    Fragment(),
    CompoundButton.OnCheckedChangeListener,
    IConfirmationDialogListener,
    IMusicFileSyncResultListener {

    private val viewModel: SongsFragmentViewModel by viewModels()
    private lateinit var bind: FragmentSongsBinding
    private lateinit var playlistDropdownAdapter: ArrayAdapter<String>
    private lateinit var songsAdapter: SongAdapter

    @Inject
    lateinit var viewUtil: ViewUtil

    @Inject
    lateinit var runningSessionManager: RunningSessionManager

    @Inject
    lateinit var locationPermissions: Array<String>

    private var switchCheckedAutomatically = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = FragmentSongsBinding.inflate(inflater, container, false)
        bind.tvActivePlaylistName.setText(viewModel.currentPlaylist().name())
        bind.tvActivePlaylistName.setSelection(viewUtil.playlistDropdownSelectedPosition)
        bind.tvActivePlaylistName.setDropDownBackgroundResource(R.color.fit_bg_blue)
        buildSongsRecyclerView(null)
        setErrorMessageVisibilities()
        setOnChangeListeners()
        bind.runningSwitch.isChecked = runningSessionManager.isRunningModeActive()
        setOnClickListeners()
        setObservers()
        return bind.root
    }

    override fun onResume() {
        super.onResume()
        setDropdownAdapter()
        bind.tvActivePlaylistName.requestFocus()
    }

    override fun onCheckedChanged(p0: CompoundButton?, checked: Boolean) {
        if (switchCheckedAutomatically) {
            switchCheckedAutomatically = false
            return
        }
        lateinit var titleText: String
        lateinit var bodyText: String
        if (checked) {
            titleText = getString(R.string.StartRunning)
            bodyText = getString(R.string.AreYouSureToStartRunning)
        } else {
            titleText = getString(R.string.EndRunning)
            bodyText = getString(R.string.AreYouSureToEndRunning)
        }
        viewUtil.createConfirmationDialog(
            titleText,
            bodyText,
            IGNORED_INDEX,
            this,
            childFragmentManager
        )
    }

    override fun onDialogConfirmed(index: Int) {
        if (bind.runningSwitch.isChecked) {
            val validationModel = viewModel.isRunningAllowed()
            val gmsEnabled = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(requireContext()) == ConnectionResult.SUCCESS

            if (!validationModel.valid) {
                rejectToStartRunningMode(
                    getString(R.string.NoSongsInPlaylist2And3),
                    getString(validationModel.errorMessageId)
                )
            } else if (!gmsEnabled) {
                rejectToStartRunningMode(
                    getString(R.string.GmsNotFound),
                    getString(R.string.GmsRequired)
                )
            } else {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    locationPermissions,
                    MAIN_LOCATION_PERMISSIONS_REQ_CODE
                )
            }
        } else {
            runningSessionManager.runningModeState.value = RunningModeState.FINISHED
        }
    }

    override fun onDialogRejected() {
        uncheckRunningSwitch()
    }

    override fun onMusicFilesSynced() {
        viewUtil.createInfoDialog(
            getString(R.string.SyncMusicFiles),
            getString(R.string.SyncMusicSuccess),
            childFragmentManager
        )
    }

    fun setDropdownAdapter() {
        playlistDropdownAdapter = ArrayAdapter(
            requireContext(),
            R.layout.layout_playlist_dropdown_item,
            viewModel.getPlaylistNames()
        )
        bind.tvActivePlaylistName.setAdapter(playlistDropdownAdapter)
    }

    private fun setOnChangeListeners() {
        bind.runningSwitch.setOnCheckedChangeListener(this)
        bind.tvActivePlaylistName.addTextChangedListener {
            changePlaylist(it.toString())
        }

        bind.etSearch.doOnTextChanged { text, start, before, _ ->
            if (start > 0) {
                buildSongsRecyclerView(viewModel.filterSongs(text.toString()))
            } else if (before > 0) {
                resetSearch()
            }
        }
    }

    private fun setObservers() {
        viewModel.audioDataManager.currentPlaylistWithSongs.observe(viewLifecycleOwner) {
            bind.tvActivePlaylistName.setText(viewModel.currentPlaylist().name())
            buildSongsRecyclerView(null)
            viewUtil.playlistDropdownSelectedPosition =
                viewModel.audioDataManager.indexOfCurrentPlaylist()
            viewModel.prepareNewPlaylist(runningSessionManager.isRunningModeActive())
            setDropdownAdapter()
        }
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    runningSessionManager.runningModeState.collectLatest {
                        if (it == RunningModeState.REJECTED_BY_LOCATION_PERMISSION && bind.runningSwitch.isChecked) {
                            runningSessionManager.runningModeState.value =
                                RunningModeState.NOT_STARTED
                            uncheckRunningSwitch()
                        }
                    }
                }
                launch {
                    viewModel.currentSongIndex.collectLatest {
                        if (::songsAdapter.isInitialized) {
                            songsAdapter.updatePlayingSongColor(it)
                        }
                    }
                }
            }
        }
    }

    private fun setOnClickListeners() {
        bind.btnSearch.setOnClickListener {
            if (bind.layoutSearchText.visibility == View.GONE) {
                bind.layoutSearchText.visibility = View.VISIBLE
            } else {
                bind.layoutSearchText.visibility = View.GONE
                resetSearch()
            }
        }
        bind.layoutSearchText.setEndIconOnClickListener {
            if (bind.etSearch.text.toString().isNotEmpty()) {
                bind.etSearch.text?.clear()
            } else {
                bind.layoutSearchText.visibility = View.GONE
            }
            resetSearch()
        }
        bind.btnSyncMusicFiles.setOnClickListener {
            viewModel.syncSongsWithLocalStorage(this)
        }
    }

    private fun resetSearch() {
        buildSongsRecyclerView(null)
        viewModel.resetSearch()
    }

    private fun changePlaylist(playlistName: String) {
        if (::playlistDropdownAdapter.isInitialized) {
            viewModel.changePlaylist(playlistDropdownAdapter.getPosition(playlistName))
        }
        setErrorMessageVisibilities()
    }

    private fun buildSongsRecyclerView(mediaSources: ConcatenatingMediaSource?) {
        val concatenatingMediaSource =
            mediaSources ?: viewModel.currentPlaylist().concatenatingMediaSource()
        songsAdapter =
            SongAdapter(concatenatingMediaSource, viewModel, viewModel.currentSongIndex.value)
        bind.rclSongs.adapter = songsAdapter
    }

    private fun setErrorMessageVisibilities() {
        if (!viewModel.currentPlaylist().isEmpty()) {
            bind.layoutErrorMessages.visibility = View.GONE
        } else {
            bind.layoutErrorMessages.visibility = View.VISIBLE
            if (viewModel.isFirstPlaylistEmpty()) {
                bind.tvNoMusicFound.text = getString(R.string.NoMusicFoundInStorage)
                bind.tvLoadMusic.visibility = View.VISIBLE
                bind.btnSyncMusicFiles.visibility = View.VISIBLE
            } else {
                bind.tvNoMusicFound.text = getString(R.string.NoSongsInPlaylist)
                bind.tvLoadMusic.visibility = View.GONE
                bind.btnSyncMusicFiles.visibility = View.GONE
            }
        }
    }

    private fun rejectToStartRunningMode(titleText: String, bodyText: String) {
        viewUtil.createInfoDialog(titleText, bodyText, childFragmentManager)
        uncheckRunningSwitch()
    }

    private fun uncheckRunningSwitch() {
        switchCheckedAutomatically = true
        bind.runningSwitch.isChecked = !bind.runningSwitch.isChecked
    }
}
