package com.dobra.fitmusic.extension

import com.google.android.exoplayer2.source.MediaSource

fun MediaSource.durationText(): String = this.mediaItem.mediaMetadata.description.toString()

fun MediaSource.name(): String = this.mediaItem.mediaMetadata.title.toString()
