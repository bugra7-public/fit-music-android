package com.dobra.fitmusic.extension

import java.util.*

fun Float.Companion.roundTo2FractionalDigits(float: Float) =
    "%.1f".format(Locale.US, float).toFloat()
