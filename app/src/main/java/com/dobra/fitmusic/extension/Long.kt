package com.dobra.fitmusic.extension

import android.content.ContentUris
import android.net.Uri
import android.provider.MediaStore

fun Long.Companion.createUri(id: Long): Uri {
    return ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id)
}
