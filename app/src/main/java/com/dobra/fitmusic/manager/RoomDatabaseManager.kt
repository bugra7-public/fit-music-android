package com.dobra.fitmusic.manager

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.dobra.fitmusic.dao.IPlaylistDao
import com.dobra.fitmusic.dao.IPlaylistSongCrossRefDao
import com.dobra.fitmusic.dao.IRunningSessionDao
import com.dobra.fitmusic.dao.ISongDao
import com.dobra.fitmusic.model.Playlist
import com.dobra.fitmusic.model.RunningMoment
import com.dobra.fitmusic.model.RunningSession
import com.dobra.fitmusic.model.Song
import com.dobra.fitmusic.model.relation.PlaylistSongCrossRef

private const val ROOM_DB_VERSION = 1
private const val ROOM_DB_NAME = "FIT_MUSIC_ROOM_DB"

@Database(
    entities = [
        Playlist::class,
        Song::class,
        PlaylistSongCrossRef::class,
        RunningSession::class,
        RunningMoment::class
    ],
    version = ROOM_DB_VERSION
)

abstract class RoomDatabaseManager : RoomDatabase() {
    abstract val playlistDao: IPlaylistDao
    abstract val songDao: ISongDao
    abstract val playlistSongCrossRefDao: IPlaylistSongCrossRefDao
    abstract val runningSessionDao: IRunningSessionDao

    companion object {
        @Volatile
        private var roomDatabaseManagerInstance: RoomDatabaseManager? = null

        fun getInstance(context: Context): RoomDatabaseManager {
            synchronized(this) {
                return roomDatabaseManagerInstance ?: Room.databaseBuilder(
                    context.applicationContext,
                    RoomDatabaseManager::class.java,
                    ROOM_DB_NAME
                ).build().also {
                    roomDatabaseManagerInstance = it
                }
            }
        }
    }
}
