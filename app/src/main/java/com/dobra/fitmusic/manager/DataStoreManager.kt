package com.dobra.fitmusic.manager

import android.content.Context
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import com.dobra.fitmusic.common.DataStoreKeys.DATASTORE_MAIN_KEY
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class DataStoreManager(val context: Context) {
    private val Context.preferencesDataStore by preferencesDataStore(DATASTORE_MAIN_KEY)
    private val dataStore = context.preferencesDataStore

    suspend fun saveBoolean(key: Preferences.Key<Boolean>, value: Boolean) {
        dataStore.edit {
            it[key] = value
        }
    }

    suspend fun saveFloat(key: Preferences.Key<Float>, value: Float) {
        dataStore.edit {
            it[key] = value
        }
    }

    suspend fun saveLong(key: Preferences.Key<Long>, value: Long) {
        dataStore.edit {
            it[key] = value
        }
    }

    suspend fun saveInteger(key: Preferences.Key<Int>, value: Int) {
        dataStore.edit {
            it[key] = value
        }
    }

    suspend fun readBoolean(key: Preferences.Key<Boolean>): Boolean? {
        return dataStore.data.map { it[key] }.first()
    }

    suspend fun readFloat(key: Preferences.Key<Float>): Float? {
        return dataStore.data.map { it[key] }.first()
    }

    suspend fun readLong(key: Preferences.Key<Long>): Long? {
        return dataStore.data.map { it[key] }.first()
    }

    suspend fun readInteger(key: Preferences.Key<Int>): Int? {
        return dataStore.data.map { it[key] }.first()
    }
}

