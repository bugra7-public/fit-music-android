package com.dobra.fitmusic.manager

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.dobra.fitmusic.common.FAST_PLAYLIST_INDEX
import com.dobra.fitmusic.common.PlaylistType
import com.dobra.fitmusic.common.SLOW_PLAYLIST_INDEX
import com.dobra.fitmusic.extension.createUri
import com.dobra.fitmusic.model.Song
import com.dobra.fitmusic.model.relation.PlaylistWithSongs
import com.dobra.fitmusic.util.CalculationUtil
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.MediaMetadata
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSource

class AudioDataManager(
    context: Context,
    private val calculationUtil: CalculationUtil
) {
    lateinit var allPlaylistsWithSongs: MutableList<PlaylistWithSongs>
    var currentPlaylistWithSongs = MutableLiveData<PlaylistWithSongs>()

    private val dataSource = DefaultDataSource.Factory(context)

    fun createSingleMediaSource(song: Song): MediaSource {
        val mediaMetadata = MediaMetadata.Builder()
            .setTitle(song.songName)
            .setDescription(calculationUtil.convertMillisToDurationText(song.duration))
            .build()

        val mediaItem = MediaItem.Builder()
            .setMediaMetadata(mediaMetadata)
            .setUri(Long.createUri(song.songUriId))
            .build()

        return ProgressiveMediaSource.Factory(dataSource).createMediaSource(mediaItem)
    }

    fun setConcatenatingMediaSourcesOfPlaylists() {
        allPlaylistsWithSongs.forEach {
            it.playlist.concatenatingMediaSource =
                createConcatenatingMediaSource(it.songs)
        }
    }

    fun createConcatenatingMediaSource(songList: List<Song>): ConcatenatingMediaSource {
        val concatenatingMediaSource = ConcatenatingMediaSource()
        val mediaItems: MutableList<MediaSource> = mutableListOf()

        songList.forEach {
            mediaItems.add(createSingleMediaSource(it))
        }
        concatenatingMediaSource.addMediaSources(mediaItems)
        return concatenatingMediaSource
    }

    fun updateConcatenatingMediaSourceOfPlaylist(playlistWithSongs: PlaylistWithSongs) {
        playlistWithSongs.playlist.concatenatingMediaSource =
            createConcatenatingMediaSource(playlistWithSongs.songs)
    }

    fun currentPlaylistType(): PlaylistType {
        return when (currentPlaylistWithSongs.value) {
            slowPlaylist() -> PlaylistType.SLOW
            fastPlaylist() -> PlaylistType.FAST
            else -> PlaylistType.OTHER
        }
    }

    fun slowPlaylist(): PlaylistWithSongs {
        return allPlaylistsWithSongs[SLOW_PLAYLIST_INDEX]
    }

    fun fastPlaylist(): PlaylistWithSongs {
        return allPlaylistsWithSongs[FAST_PLAYLIST_INDEX]
    }

    fun indexOfCurrentPlaylist(): Int {
        return allPlaylistsWithSongs.indexOf(currentPlaylistWithSongs.value)
    }
}
