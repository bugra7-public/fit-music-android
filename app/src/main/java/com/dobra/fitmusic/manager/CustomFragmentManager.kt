package com.dobra.fitmusic.manager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.dobra.fitmusic.R
import com.dobra.fitmusic.fragment.PlaylistFragment
import com.dobra.fitmusic.fragment.ProfileFragment
import com.dobra.fitmusic.fragment.SongsFragment

class CustomFragmentManager {
    private val songsFragmentInstance = SongsFragment()
    private var currentFragmentInstance: Fragment = songsFragmentInstance

    fun loadFragment(fragment: Fragment?, fragmentManager: FragmentManager) {
        if (fragment != null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.hide(currentFragmentInstance)
            currentFragmentInstance = fragment
            if (!fragment.isAdded) {
                fragmentTransaction.add(R.id.fragmentContainer, fragment)
                if (!isBottomNavigationFragment(fragment)) {
                    addFragmentToBackStack(fragment, fragmentTransaction)
                }
            }
            fragmentTransaction.show(fragment)
            fragmentTransaction.commit()
        }
    }

    fun loadPlaylistsFragment(fragmentManager: FragmentManager) {
        loadFragment(PlaylistFragment(), fragmentManager)
    }

    fun loadSongsFragment(fragmentManager: FragmentManager) {
        try {
            songsFragmentInstance.setDropdownAdapter()
        } catch (ignored: Exception) {
            // To prevent "Not attached to context" error on the first call
        }
        loadFragment(songsFragmentInstance, fragmentManager)
    }

    fun loadProfileFragment(fragmentManager: FragmentManager) {
        loadFragment(ProfileFragment(), fragmentManager)
    }

    fun isOnBottomNavigationFragment(): Boolean {
        return isBottomNavigationFragment(currentFragmentInstance)
    }

    fun goBackToPreviousFragment(fragmentManager: FragmentManager) {
        fragmentManager.popBackStackImmediate()
        loadFragment(fragmentManager.fragments.lastOrNull(), fragmentManager)
    }

    private fun addFragmentToBackStack(
        fragment: Fragment,
        fragmentTransaction: FragmentTransaction
    ) {
        fragmentTransaction.addToBackStack(fragment.id.toString())
    }

    private fun isBottomNavigationFragment(fragment: Fragment): Boolean {
        return fragment is PlaylistFragment || fragment is SongsFragment || fragment is ProfileFragment
    }
}
