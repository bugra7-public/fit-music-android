package com.dobra.fitmusic.manager

import com.dobra.fitmusic.dao.IPlaylistDao
import com.dobra.fitmusic.model.Song
import com.dobra.fitmusic.model.relation.id
import com.dobra.fitmusic.util.MusicFileUtil
import kotlinx.coroutines.runBlocking

class SongSyncManager(
    private val musicFileUtil: MusicFileUtil,
    private val audioDataManager: AudioDataManager,
    private val fitMusicDao: IPlaylistDao
) {

    fun syncSongsWithLocalStorage(musicFileSyncResultListener: IMusicFileSyncResultListener) {
        val newAllSongs = musicFileUtil.getAllSongsFromStorage()
        removeDeletedSongs(newAllSongs)
        addNewSongs(newAllSongs)
        audioDataManager.updateConcatenatingMediaSourceOfPlaylist(audioDataManager.allPlaylistsWithSongs[0])
        refreshCurrentPlaylistForSongRecycler()
        musicFileSyncResultListener.onMusicFilesSynced()
    }

    private fun refreshCurrentPlaylistForSongRecycler() {
        val currentPlaylist = audioDataManager.currentPlaylistWithSongs.value
        audioDataManager.currentPlaylistWithSongs.value = audioDataManager.slowPlaylist()
        audioDataManager.currentPlaylistWithSongs.value = audioDataManager.fastPlaylist()
        currentPlaylist?.songs?.sortBy { song -> song.songName.lowercase() }
        audioDataManager.setConcatenatingMediaSourcesOfPlaylists()
        audioDataManager.currentPlaylistWithSongs.value = currentPlaylist
    }

    private fun addNewSongs(newAllSongs: MutableList<Song>) {
        runBlocking {
            for (song in newAllSongs) {
                if (!audioDataManager.allPlaylistsWithSongs[0].songs.contains(song)) {
                    audioDataManager.allPlaylistsWithSongs[0].songs.add(song)
                    fitMusicDao.addNewSongsWithCascade(
                        song,
                        audioDataManager.allPlaylistsWithSongs[0].id()
                    )
                }
            }
        }
    }

    private fun removeDeletedSongs(newAllSongs: MutableList<Song>) {
        runBlocking {
            val indexesToBeDeleted = arrayListOf<Int>()
            for (i in audioDataManager.allPlaylistsWithSongs[0].songs.indices) {
                val song = audioDataManager.allPlaylistsWithSongs[0].songs[i]
                if (!newAllSongs.contains(song)) {
                    indexesToBeDeleted.add(i)
                    fitMusicDao.deleteSongWithCascade(song)
                }
            }
            for (i in indexesToBeDeleted.asReversed()) {
                audioDataManager.allPlaylistsWithSongs[0].songs.removeAt(i)
            }
        }
    }
}

interface IMusicFileSyncResultListener {
    fun onMusicFilesSynced()
}
