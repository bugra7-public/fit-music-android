package com.dobra.fitmusic.manager

import com.dobra.fitmusic.common.PlaylistType
import com.dobra.fitmusic.common.highSpeedThreshold
import com.dobra.fitmusic.common.lowSpeedThreshold
import com.dobra.fitmusic.model.relation.PlaylistWithSongs
import com.dobra.fitmusic.model.relation.concatenatingMediaSource
import com.dobra.fitmusic.model.relation.size

private const val TEMP_SPEED_LIST_SIZE_LIMIT = 9
private const val EVALUATED_SPEED_LIMIT = 50F

class AutomaticSongChangeManager(
    private val musicPlayerManager: MusicPlayerManager,
    private val audioDataManager: AudioDataManager
) {
    private val tempSpeedList = mutableListOf<Float>()

    fun clearSpeedValues() {
        tempSpeedList.clear()
    }

    fun changeSongAccordingToSpeed(newSpeed: Float) {
        if (newSpeed > EVALUATED_SPEED_LIMIT) {
            return
        }
        addNewSpeedToQueue(newSpeed)
        val recentAverageSpeed = calculateRecentAverageSpeed(tempSpeedList)
        val playlistType = audioDataManager.currentPlaylistType()

        if (recentAverageSpeed > highSpeedThreshold && playlistType != PlaylistType.FAST) {
            changePlaylistDuringRunningSession(audioDataManager.fastPlaylist())
        } else if (recentAverageSpeed < lowSpeedThreshold && playlistType != PlaylistType.SLOW) {
            changePlaylistDuringRunningSession(audioDataManager.slowPlaylist())
        }
    }

    private fun addNewSpeedToQueue(newSpeed: Float) {
        tempSpeedList.add(newSpeed)
        if (tempSpeedList.size >= TEMP_SPEED_LIST_SIZE_LIMIT) {
            tempSpeedList.removeFirst()
        }
    }

    private fun calculateRecentAverageSpeed(tempSpeedList: MutableList<Float>): Float {
        if (tempSpeedList.isEmpty()) {
            return 0F
        }
        return tempSpeedList.reduce { acc, next -> acc + next } / tempSpeedList.size
    }

    private fun changePlaylistDuringRunningSession(playlist: PlaylistWithSongs) {
        musicPlayerManager.startSong(
            concatenatingMediaSource = playlist.concatenatingMediaSource(),
            index = createRandomNumber(playlist.size()),
            playWhenReady = false
        )
        audioDataManager.currentPlaylistWithSongs.postValue(playlist)
    }

    private fun createRandomNumber(excludedMax: Int): Int {
        return (0 until excludedMax).random()
    }
}
