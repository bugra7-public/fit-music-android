package com.dobra.fitmusic.manager

import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Build
import com.dobra.fitmusic.R
import com.dobra.fitmusic.common.ZERO
import com.dobra.fitmusic.model.RunningModeState
import com.dobra.fitmusic.model.RunningMoment
import com.dobra.fitmusic.model.RunningSession
import com.dobra.fitmusic.model.relation.RunningSessionWithMoments
import com.dobra.fitmusic.service.RunningSessionService
import com.dobra.fitmusic.util.CalculationUtil
import com.google.android.exoplayer2.util.Util
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

private const val NOT_EVALUATED_LOCATION_COUNT = 3

class RunningSessionManager(
    private val context: Context,
    private val calculationUtil: CalculationUtil
) : ILocationListener {
    private val locationManager = LocationManager(context, this)

    private val _currentSpeed: MutableStateFlow<Float> = MutableStateFlow(ZERO)
    val currentSpeed: StateFlow<Float> = _currentSpeed

    var runningModeState: MutableStateFlow<RunningModeState> =
        MutableStateFlow(RunningModeState.NOT_STARTED)

    private var runningSessionStartTime: String? = null
    private val runningMoments = arrayListOf<RunningMoment>()
    private var totalDistance = ZERO
    private var locationCount = 0

    override fun onLocationChanged(latestLocation: Location) {
        if (locationCount < NOT_EVALUATED_LOCATION_COUNT) {
            locationCount++
            return
        }
        val previousRunningMoment = runningMoments.getOrNull(runningMoments.size - 1)
        if (previousRunningMoment?.time == latestLocation.time) return

        val distanceSinceLastMoment = calculationUtil.distance(
            previousRunningMoment?.latitude,
            previousRunningMoment?.longitude,
            latestLocation.latitude,
            latestLocation.longitude
        )
        _currentSpeed.value = calculationUtil.speed(
            distanceSinceLastMoment,
            previousRunningMoment?.time,
            latestLocation.time
        )

        totalDistance += distanceSinceLastMoment

        if (runningSessionStartTime == null) {
            runningSessionStartTime = calculationUtil.createFormattedDateString(latestLocation.time)
        }
        runningSessionStartTime?.let { sessionStartTime ->
            runningMoments.add(
                RunningMoment(
                    latestLocation.time,
                    latestLocation.latitude,
                    latestLocation.longitude,
                    sessionStartTime
                )
            )
        }
    }

    fun isRunningModeActive(): Boolean {
        return runningModeState.value == RunningModeState.RUNNING ||
            runningModeState.value == RunningModeState.PAUSED
    }

    fun startRunningSession() {
        startRunningSessionService()
        locationManager.startLocationRequests()
    }

    fun endRunningSession(): RunningSessionWithMoments? {
        stopRunningSessionService()
        locationManager.removeLocationUpdates()

        val runningSessionWithMoments = createRunningSessionModel()
        resetRunningSessionValues()
        return runningSessionWithMoments
    }

    private fun startRunningSessionService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Util.startForegroundService(context, Intent(context, RunningSessionService::class.java))
        } else {
            context.startService(Intent(context, RunningSessionService::class.java))
        }
    }

    private fun createRunningSessionModel(): RunningSessionWithMoments? {
        if (runningMoments.isNotEmpty()) {
            val startTime = runningMoments[0].time
            val endTime = runningMoments[runningMoments.size - 1].time
            val (minutes, seconds) = calculationUtil.calculateDuration(startTime, endTime)

            runningSessionStartTime?.let {
                return RunningSessionWithMoments(
                    RunningSession(
                        it,
                        context.getString(
                            R.string.RunningDuration,
                            minutes,
                            seconds
                        ),
                        totalDistance.toInt().toString(),
                        calculationUtil.speed(totalDistance, startTime, endTime).toString()
                    ),
                    // toMutableList is for passing by value instead of reference
                    runningMoments.toMutableList()
                )
            }
        }
        return null
    }

    private fun stopRunningSessionService() {
        context.stopService(Intent(context, RunningSessionService::class.java))
    }

    private fun resetRunningSessionValues() {
        runningMoments.clear()
        totalDistance = ZERO
        _currentSpeed.value = ZERO
        runningSessionStartTime = null
        locationCount = 0
    }
}
