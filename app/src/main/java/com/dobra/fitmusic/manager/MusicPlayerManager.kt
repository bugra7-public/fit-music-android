package com.dobra.fitmusic.manager

import android.content.Context
import android.content.Intent
import android.os.Build
import com.dobra.fitmusic.service.MusicPlayerService
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.exoplayer2.util.Util

class MusicPlayerManager(
    val context: Context
) {
    val exoPlayer = ExoPlayer.Builder(context).build().apply {
        this.repeatMode = Player.REPEAT_MODE_ALL
        setHandleAudioBecomingNoisy(true)
        setAudioAttributes(
            AudioAttributes.DEFAULT,
            true
        )
    }

    fun startSong(
        concatenatingMediaSource: ConcatenatingMediaSource,
        index: Int,
        playWhenReady: Boolean
    ) {
        startMusicPlayerService()

        setMusicPlayer(concatenatingMediaSource, index, 0)

        if (playWhenReady) exoPlayer.playWhenReady = true
    }

    fun pauseSong() {
        if (exoPlayer.isPlaying) {
            exoPlayer.pause()
        }
    }

    fun continueSong() {
        startMusicPlayerService()
        exoPlayer.play()
    }

    fun playNextSong() {
        exoPlayer.seekToNextMediaItem()
    }

    fun playPreviousSong() {
        exoPlayer.seekToPreviousMediaItem()
    }

    fun isPlaying(): Boolean {
        return exoPlayer.isPlaying
    }

    fun setMusicPlayer(
        concatenatingMediaSource: ConcatenatingMediaSource,
        songIndex: Int,
        positionMs: Long
    ) {
        exoPlayer.setMediaSource(concatenatingMediaSource)
        exoPlayer.prepare()
        exoPlayer.seekTo(songIndex, positionMs)
    }

    private fun startMusicPlayerService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Util.startForegroundService(context, Intent(context, MusicPlayerService::class.java))
        } else {
            context.startService(Intent(context, MusicPlayerService::class.java))
        }
    }
}
