package com.dobra.fitmusic.common

import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.floatPreferencesKey
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.longPreferencesKey

const val ZERO = 0F

const val IGNORED_INDEX = -1
const val INFO_DIALOG_TAG = "INFO_DIALOG_TAG"
const val CONFIRMATION_DIALOG_TAG = "CONFIRMATION_DIALOG_TAG"

private const val LOW_SPEED_THRESHOLD = 5.5F
private const val HIGH_SPEED_THRESHOLD = 10F

var lowSpeedThreshold = LOW_SPEED_THRESHOLD
var highSpeedThreshold = HIGH_SPEED_THRESHOLD

const val SLOW_PLAYLIST_INDEX = 1
const val FAST_PLAYLIST_INDEX = 2

enum class PlaylistType {
    SLOW, FAST, OTHER
}

object DataStoreKeys {
    const val DATASTORE_MAIN_KEY = "FIT_MUSIC_DATASTORE_MAIN_KEY"

    val KEY_REPEAT_PRESSED = booleanPreferencesKey("KEY_REPEAT_PRESSED")
    val KEY_SHUFFLE_PRESSED = booleanPreferencesKey("KEY_SHUFFLE_PRESSED")
    val KEY_LOW_SPEED_THRESHOLD = floatPreferencesKey("KEY_LOW_SPEED_THRESHOLD")
    val KEY_HIGH_SPEED_THRESHOLD = floatPreferencesKey("KEY_HIGH_SPEED_THRESHOLD")
    val KEY_CURRENT_PLAYLIST_INDEX = intPreferencesKey("KEY_CURRENT_PLAYLIST_INDEX")
    val KEY_CURRENT_SONG_INDEX = intPreferencesKey("KEY_CURRENT_SONG_INDEX")
    val KEY_CURRENT_SONG_TIME_POSITION_IN_MS =
        longPreferencesKey("KEY_CURRENT_SONG_TIME_POSITION_IN_MS")
}
