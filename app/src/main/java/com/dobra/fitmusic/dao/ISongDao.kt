package com.dobra.fitmusic.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.dobra.fitmusic.model.Song
import com.dobra.fitmusic.model.relation.PlaylistSongCrossRef

@Dao
interface ISongDao : IPlaylistSongCrossRefDao {

    suspend fun addNewSongsWithCascade(song: Song, playlistId: Int) {
        insertSong(song)
        insertPlaylistSongCrossRef(
            PlaylistSongCrossRef(
                playlistId = playlistId,
                songUriId = song.songUriId
            )
        )
    }

    suspend fun deleteSongWithCascade(song: Song) {
        deletePlaylistSongCrossRefBySongId(song.songUriId)
        deleteSong(song)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSong(song: Song)

    @Delete
    suspend fun deleteSong(song: Song)
}
