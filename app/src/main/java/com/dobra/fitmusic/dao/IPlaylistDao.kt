package com.dobra.fitmusic.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.dobra.fitmusic.model.Playlist
import com.dobra.fitmusic.model.relation.PlaylistSongCrossRef
import com.dobra.fitmusic.model.relation.PlaylistWithSongs
import com.dobra.fitmusic.model.relation.id
import com.dobra.fitmusic.model.relation.name
import com.dobra.fitmusic.model.relation.setId

@Dao
interface IPlaylistDao : ISongDao {
    suspend fun savePlaylistToLocalDatabase(
        playlistWithSongs: PlaylistWithSongs,
        automaticallyCreateSongsInDatabase: Boolean
    ) {
        insertPlaylist(playlistWithSongs.playlist)

        playlistWithSongs.setId(getPlaylistId(playlistWithSongs.name()))

        playlistWithSongs.songs.forEach { song ->
            if (automaticallyCreateSongsInDatabase) insertSong(song)

            insertPlaylistSongCrossRef(
                PlaylistSongCrossRef(
                    playlistWithSongs.id(),
                    song.songUriId
                )
            )
        }
    }

    suspend fun deletePlaylistFromLocalDatabase(playlistWithSongs: PlaylistWithSongs) {
        deletePlaylist(playlistWithSongs.playlist)

        playlistWithSongs.songs.forEach { song ->
            deletePlaylistSongCrossRef(
                PlaylistSongCrossRef(
                    playlistWithSongs.id(),
                    song.songUriId
                )
            )
        }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPlaylist(playlist: Playlist)

    @Query("SELECT playlistId FROM Playlist WHERE playlistName IS :playlistName")
    suspend fun getPlaylistId(playlistName: String): Int

    @Query("SELECT * FROM Playlist")
    suspend fun getPlaylistsWithSongs(): List<PlaylistWithSongs>

    @Update
    suspend fun updatePlaylist(playlist: Playlist)

    @Delete
    suspend fun deletePlaylist(playlist: Playlist)
}
