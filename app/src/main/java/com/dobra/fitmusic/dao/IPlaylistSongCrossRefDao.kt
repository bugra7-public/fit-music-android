package com.dobra.fitmusic.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dobra.fitmusic.model.relation.PlaylistSongCrossRef

@Dao
interface IPlaylistSongCrossRefDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPlaylistSongCrossRef(playlistSongCrossRef: PlaylistSongCrossRef)

    @Delete
    suspend fun deletePlaylistSongCrossRef(playlistSongCrossRef: PlaylistSongCrossRef)

    @Query("DELETE FROM PlaylistSongCrossRef WHERE songUriId IS :songUriId")
    suspend fun deletePlaylistSongCrossRefBySongId(songUriId: Long)
}
