package com.dobra.fitmusic.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dobra.fitmusic.model.RunningMoment
import com.dobra.fitmusic.model.RunningSession
import com.dobra.fitmusic.model.relation.RunningSessionWithMoments
import kotlinx.coroutines.flow.Flow

@Dao
interface IRunningSessionDao {

    suspend fun saveRunningSessionToLocalDatabase(runningSessionWithMoments: RunningSessionWithMoments) {
        insertRunningSession(runningSessionWithMoments.runningSession)

        runningSessionWithMoments.runningMoments.forEach {
            insertRunningMoment(it)
        }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRunningSession(runningSession: RunningSession)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRunningMoment(runningMoment: RunningMoment)

    @Query("SELECT * FROM RunningSession")
    fun getRunningSessionWithMoments(): Flow<List<RunningSessionWithMoments>>
}
